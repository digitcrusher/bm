# bm - The bookmarks.md manager
# Copyright (C) 2022-2023, 2025 Karol "digitcrusher" Łacina
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import re
from dataclasses import dataclass, field
from datetime import datetime, timezone
from enum import Enum
from typing import Optional, Union

from common import fuzzy_match, Logger, MusicMetadata, slugify

@dataclass
class Node:
  name: str
  added: list[str] = field(default_factory=list, init=False)
  attr_music: bool | None = field(default=None, init=False)
  attr_discard_video: bool | None = field(default=None, init=False)
  unknown_attrs: list[str] = field(default_factory=list, init=False)
  src_line: int | None = field(default=None, init=False)
  parent: Optional['Category'] = field(default=None, init=False)

  @property
  def slug(self):
    return slugify(self.name)

  @property
  def is_music(self):
    if self.attr_music is not None:
      return self.attr_music
    elif self.parent is not None:
      return self.parent.is_music
    else:
      return False

  @property
  def should_discard_video(self):
    if self.attr_discard_video is not None:
      return self.attr_discard_video
    elif self.attr_music is not None:
      return self.attr_music
    elif self.parent is not None:
      return self.parent.should_discard_video
    else:
      return False

  def add_attr(self, string, *, log=Logger()):
    with log.line_num(self.src_line):
      if string.startswith('added to Firefox'):
        val = string.removeprefix('added to Firefox').lstrip()
        if not val:
          log.error("Expected a timestamp after 'added to Firefox'")

        for timestamp in map(int, val.split(',')):
          date = datetime.fromtimestamp(timestamp, tz=timezone.utc)
          formatted = date.strftime('%Y-%m-%d %H:%M:%SZ')
          self.added.append(formatted)

      elif string.startswith('added to Chrome'):
        val = string.removeprefix('added to Chrome').lstrip()
        if not val:
          log.error("Expected a timestamp after 'added to Chrome'")

        for timestamp in map(int, val.split(',')):
          date = datetime.fromtimestamp((timestamp - 11_644_473_600_000_000) / 1_000_000, tz=timezone.utc)
          formatted = date.strftime('%Y-%m-%d %H:%M:%S.%fZ')
          self.added.append(formatted)

      elif string.startswith('added'):
        val = string.removeprefix('added').lstrip()
        if not val:
          log.error("Expected a date after 'added'")

        self.added += [date.strip() for date in val.split(',')]

      elif string == 'music':
        self.attr_music = True

      elif string == 'not music':
        self.attr_music = False

      elif string == 'discard video':
        self.attr_discard_video = True

      elif string == 'keep video':
        self.attr_discard_video = False

      else:
        log.warn(f'Unknown attribute: {string!r}')
        self.unknown_attrs.append(string)

  @property
  def attrs_str(self):
    result = ''
    if self.added:
      result += ' {added ' + ', '.join(self.added) + '}'
    if self.attr_music is not None:
      result += ' {music}' if self.attr_music else ' {not music}'
    if self.attr_discard_video is not None:
      result += ' {discard video}' if self.attr_discard_video else ' {keep video}'
    for attr in self.unknown_attrs:
      result += ' {' + attr + '}'
    return result

def split_off_attrs(string):
  result = []

  string = string[::-1]
  regex = re.compile(r'\s*\}(.*?)\{')
  while (match := regex.match(string)) is not None:
    new_str = string[match.end():]
    stripped = new_str.strip()
    if stripped == '-' or stripped == '#' * len(stripped):
      break
    string = new_str

    result.append(match[1][::-1].strip())

  string = string[::-1]
  result.reverse()
  return (string, result)

@dataclass
class Bookmark(Node):
  links: list[tuple[str, str]]

  @staticmethod
  def parse(string, src_line=None, *, log=Logger()):
    result = Bookmark(None, [])
    result.src_line = src_line

    with log.line_num(result.src_line):
      string, attrs = split_off_attrs(string)
      for attr in attrs:
        result.add_attr(attr, log=log)

      string = string[::-1]
      link_regex = re.compile(r'\s*\)(.*?)\(\]\](.*?)\[\[')
      while True:
        match = link_regex.match(string)
        if match is None:
          match = re.match(r'\s*\).*?\(\].*\[(.*)-\s*$', string)
          if match is not None and match[1].strip():
            log.warn('Found a possible incorrectly formatted link')
          break

        new_str = string[match.end():]
        if new_str.strip() == '-':
          break
        string = new_str

        result.links.append((match[2][::-1].strip(), match[1][::-1].strip()))

      string = string[::-1].lstrip()
      if not string.startswith('-'):
        log.error("Bookmark doesn't have a leading dash")
      string = string.removeprefix('-').strip()

      name_link = re.match(r'\[(.*)\]\((.*)\)', string)
      if name_link is not None:
        result.name = name_link[1].strip()
        result.links.append((None, name_link[2].strip()))
      else:
        result.name = string

      result.links.reverse()

      if not result.links:
        log.warn('Found a bookmark without any links')

    return result

  def __str__(self, *, log=Logger()):
    name_str = None

    links_str = ''
    for site, target in self.links:
      if site is None:
        if name_str is not None:
          with log.line_num(self.src_line):
            log.error('Bookmark contains more than one name link')
        name_str = f'[{self.name}]({target})'
      else:
        links_str += f' [[{site}]]({target})'

    if name_str is None:
      name_str = self.name

    return '- ' + name_str + links_str + self.attrs_str

  def __getitem__(self, key):
    for site, target in self.links:
      if site == key:
        return target
    raise KeyError(repr(key))

  def __contains__(self, key):
    return key in map(lambda x: x[0], self.links)

  def music_metadata(self, *, log=Logger()):
    with log.line_num(self.src_line):
      return MusicMetadata.parse(self.name, log=log)

  def fuzzy_search(self, pattern, limit=None):
    match = fuzzy_match(pattern, self.name)
    return [] if match is None else [(self, *match)]

  def walk(self, callback):
    callback(self)

@dataclass
class Category(Node):
  desc: str
  items: list[Union[Bookmark, 'Category']]

  def __str__(self, depth=0, *, log=Logger()):
    result = ''

    if depth > 0:
      result = '#' * depth + ' ' + self.name + self.attrs_str + '\n'

    if self.desc:
      result += self.desc + '\n'

    for item in self.items:
      if isinstance(item, Category):
        if not result.endswith('\n\n'):
          result += '\n'
        result += item.__str__(depth + 1, log=log)
      else:
        result += item.__str__(log=log) + '\n'

    return result

  def __getitem__(self, idx):
    return self.items[idx]

  def __bool__(self):
    return bool(self.items)

  def append(self, item):
    self.items.append(item)
    item.parent = self

  class SortOrder(Enum):
    Name = 'name'
    Added = 'added'

  def sort(self, order, reverse=False, recurse=False):
    if recurse:
      for item in self.items:
        if isinstance(item, Category):
          item.sort(order, reverse, recurse)

    match order:
      case Category.SortOrder.Name:
        self.items.sort(key=lambda x: x.name, reverse=reverse)
      case Category.SortOrder.Added:
        self.items.sort(key=lambda x: x.added, reverse=reverse)

    a, b = [], []
    for item in self.items:
      if isinstance(item, Category):
        b.append(item)
      else:
        a.append(item)
    self.items = a + b

  def fuzzy_search(self, pattern, limit=None):
    results = []

    for item in self.items:
      results += item.fuzzy_search(pattern, limit)

    results.sort(key=lambda x: x[1], reverse=True)
    if limit is not None:
      results = results[:limit]

    return results

  def walk(self, callback):
    for item in self.items:
      item.walk(callback)

@dataclass
class Bookmarks:
  root: Category
  by_slug: dict[str, Bookmark]

  @staticmethod
  def parse(string, *, log=Logger()):
    result = Bookmarks(Category('', '', []), {})

    category_stack = [result.root]

    def push_category(category):
      category_stack[-1].append(category)
      category_stack.append(category)

    def pop_category():
      category = category_stack.pop()

      category.desc = category.desc.lstrip('\n').rstrip()
      if category.desc:
        category.desc += '\n'

    for i, line in enumerate(string.splitlines()):
      with log.line_num(i + 1):
        try:
          back_log = Logger()
          bookmark = Bookmark.parse(line, i + 1, log=back_log)

        except Logger.Error:
          line, attrs = split_off_attrs(line)
          hashes, space, name = line.lstrip().partition(' ')
          depth = len(hashes)
          if space and hashes == '#' * depth:
            category = Category(name.strip(), '', [])
            category.src_line = i + 1
            for attr in attrs:
              category.add_attr(attr, log=log)

            if depth > len(category_stack):
              log.error("Category's depth is too high")
            else:
              while depth < len(category_stack):
                pop_category()
              push_category(category)

          else:
            if category_stack[-1] and line.strip():
              log.warn('Found a non-empty category description line after first bookmark')
            category_stack[-1].desc += line + '\n'

        except Exception as e:
          log.error(log.except_msg(e, 'parsing bookmark'))

        else:
          back_log.flush_into(log)
          category_stack[-1].append(bookmark)
          if bookmark.slug in result.by_slug:
            log.error('Found a bookmark with an already used name')
          result.by_slug[bookmark.slug] = bookmark

    while category_stack:
      pop_category()

    return result
