# bm - The bookmarks.md manager
# Copyright (C) 2022-2023, 2025 Karol "digitcrusher" Łacina
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os, requests, string, time
from bs4 import BeautifulSoup
from urllib.parse import urlparse, parse_qs, quote

from common import Logger, MusicMetadata

def fetch_thumbnail(bookmark, app):
  if 'local' in bookmark:
    path = bookmark['local']
    if os.path.isdir(path):
      for file in os.listdir(path):
        if 'cover' in file.casefold():
          return os.path.join(path, file)

  cached_path = os.path.join(os.path.expanduser(app.cfg.cache), 'thumbnails', bookmark.slug)

  if not os.path.isfile(cached_path):
    app.log.info(f'Trying to find a thumbnail for bookmark {bookmark.name!r}')
    try:
      url = find_url(bookmark, log=app.log)
      if url is None:
        return None
    except Exception as e:
      app.log.warn(app.log.except_msg(e, 'looking for a thumbnail'))
      return None

    os.makedirs(os.path.dirname(cached_path), exist_ok=True)
    with open(cached_path, 'xb') as file:
      file.write(requests.get(url).content)

  return os.path.abspath(cached_path)

aphex_twin_covers = None

def find_url(bookmark, *, log=Logger()):
  session = requests.Session()
  session.headers['User-Agent'] = 'digitcrusher-bm/1.0'

  if 'website' in bookmark:
    url = urlparse(bookmark['website'])
    if url.netloc == 'aphextwin.warp.net':
      global aphex_twin_covers
      if aphex_twin_covers is None:
        website = BeautifulSoup(session.get('https://aphextwin.warp.net/').text, 'html.parser')
        aphex_twin_covers = website.find_all('a', class_='main-product-image artwork')

      for cover in aphex_twin_covers:
        if urlparse(cover['href']).path == url.path:
          return cover.img['src'].replace('/s/', '/l/')

      id = url.path.removeprefix('/release/')
      id = id[:-len(id.lstrip(string.digits))]
      return f'https://d1rgjmn2wmqeif.cloudfront.net/r/l/{id}.jpg'

  if 'Bandcamp' in bookmark:
    soup = BeautifulSoup(session.get(bookmark['Bandcamp']).text, 'html.parser')
    album_cover = soup.find(id='tralbumArt')
    if album_cover is not None:
      return album_cover.img['src']
    else:
      return soup.find(class_='band-photo')['src']

  metadata = bookmark.music_metadata(log=log)

  while True:
    query = []
    if metadata.author is not None:
      query.append(f'artist:{quote(metadata.author)}')
    if metadata.title is not None:
      query.append(f'release:{quote(metadata.title)}')
    if metadata.format is not None:
      try:
        types = {
          MusicMetadata.Format.Song: ('Single', None),
          MusicMetadata.Format.Album: ('Album', None),
          MusicMetadata.Format.ExtendedPlay: ('EP', None),
          MusicMetadata.Format.SongCompilation: ('Album', 'Compilation'),
          MusicMetadata.Format.AlbumCompilation: ('Album', 'Compilation'),
          MusicMetadata.Format.ExtendedPlayCompilation: ('Album', 'Compilation'),
          MusicMetadata.Format.LiveSongCompilation: ('Album', 'Live'),
        }
        primary, secondary = types[metadata.format]
        query.append(f'primarytype:{primary}')
        if secondary is not None:
          query.append(f'secondarytype:{secondary}')
      except KeyError:
        pass
    response = session.get(f'https://musicbrainz.org/ws/2/release?query={" AND ".join(query)}&fmt=json')

    if response.status_code == 503:
      time.sleep(3)
      continue
    else:
      results = response.json()['releases']
      for result in results:
        cover = session.head(f'https://coverartarchive.org/release/{result["id"]}/front')
        if cover.status_code == 307:
          return cover.headers['Location']
      break

  if 'YouTube' in bookmark:
    url = urlparse(bookmark['YouTube'])
    # There's also sddefault, hqdefault and default but they are 4:3 and not 16:9.
    qualities = ['maxresdefault', 'mqdefault', 'sddefault', 'hqdefault', 'default']
    if url.path == '/watch':
      video_id = parse_qs(url.query)['v'][0]
      for quality in qualities:
        cover = f'https://img.youtube.com/vi/{video_id}/{quality}.jpg'
        if session.head(cover).status_code == 200:
          return cover

    elif url.path == '/playlist':
      # YouTube unfortunately redirects to a privacy policy consent page
      # automatically and curl seems to be in some kind of whitelist for that.
      user_agent = session.headers['User-Agent']
      session.headers['User-Agent'] = 'curl/1.2.3'
      # We also have to clear the cookies.
      session.cookies.clear()
      response = session.get(bookmark['YouTube']).text
      session.headers['User-Agent'] = user_agent

      soup = BeautifulSoup(response, 'html.parser')
      covers = [i['content'] for i in soup.find_all('meta', property='og:image')]
      covers.reverse() # The filenames lie sometimes.
      for quality in qualities:
        for cover in covers:
          if quality in cover:
            return cover

  if 'DISCOGS_TOKEN' in os.environ:
    url = f'https://api.discogs.com/database/search?per_page=1&token={os.environ["DISCOGS_TOKEN"]}&type=master'
    if metadata.author is not None:
      url += f'&artist={quote(metadata.author)}'
    if metadata.title is not None:
      url += f'&release_title={quote(metadata.title)}'
    if metadata.format is not None:
      url += f'&format={quote(str(metadata.format))}'

    results = session.get(url).json()['results']
    if results:
      return results[0]['cover_image']

  return None
