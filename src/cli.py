# bm - The bookmarks.md manager
# Copyright (C) 2022-2023, 2025 Karol "digitcrusher" Łacina
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os, subprocess, sys

import downloads, music
from common import is_url
from downloads import cleanup_download, complete_download, download_with_ytdl, YtdlError
from model import Category
from play import format_playback_state, is_file_playable, Player

def input_choice(cnt, msg='Which one is it?'):
  result = None
  while True:
    try:
      result = int(input(msg + f' (1-{cnt}) ')) - 1
      if 0 <= result < cnt:
        break
    except ValueError:
      pass
    except:
      result = None
      print()
      break
  return result

def input_question(msg='Yes or no?'):
  while True:
    try:
      result = input(msg + ' (y/n) ').casefold()
      if result in {'y', 'n'}:
        return result == 'y'
    except:
      print()
      return None

def configure(app):
  app.save_config()

def open_(pattern, app):
  app.load_config()
  bookmarks = app.read_bookmarks()

  results = bookmarks.root.fuzzy_search(pattern, app.cfg.search_limit)

  for i, result in enumerate(results):
    bookmark, _, is_matched = result
    line = f'{i + 1}. '
    for i, c in enumerate(bookmark.name):
      line += f'\x1b[1m{c}\x1b[0m' if is_matched[i] else c
    print(line)

  choice = input_choice(len(results))
  if choice is None:
    return
  bookmark = results[choice][0]

  print()
  for i, link in enumerate(bookmark.links):
    site, target = link
    print(f'{i + 1}. ' + (target if site is None else f'[{site}]({target})'))

  choice = input_choice(len(bookmark.links))
  if choice is None:
    return
  site, target = bookmark.links[choice]
  if not target:
    app.log.error('Cannot open an empty link')

  app.load_unfinished_bookmarks()
  playback_state = app.unfinished_bookmarks.get(bookmark.slug)
  if playback_state is not None:
    should_resume = input_question(f'Do you want to resume playback at {format_playback_state(playback_state)}?')
    if should_resume is None:
      return
  else:
    should_resume = True

  if is_url(target):
    cached_path = downloads.path_for(target, app)

    if not os.path.exists(cached_path):
      if app.cfg.download:
        while True:
          try:
            download_with_ytdl(target, bookmark, app=app)
          except FileNotFoundError:
            app.log.warn('yt-dlp is not installed on your system')
          except KeyboardInterrupt:
            pass
          except YtdlError as e:
            if e.files:
              match input_choice(3, 'Do you want to 1. retry, 2. call it done or 3. cancel the download?'):
                case 0:
                  continue
                case 1:
                  complete_download(target, e.files, app)
                case 2:
                  cleanup_download(target, app)
          break
      else:
        app.log.info(f'Trying to play{" audio" if bookmark.should_discard_video else ""} with mpv: {target!r}')
        if Player(app).run(target, bookmark, (site, target), should_resume).returncode == 0:
          return

    if os.path.exists(cached_path):
      app.log.info(f'Playing{" audio" if bookmark.should_discard_video else ""} with mpv: {target!r}')
      Player(app).run(cached_path, bookmark, (site, target), should_resume)
    elif input_question('Are you sure you want to open this URL in the browser?'):
      app.log.info(f'Opening {target!r}')
      subprocess.run(['xdg-open', target])

  else:
    if bookmark.is_music or is_file_playable(target):
      app.log.info(f'Playing{" audio" if bookmark.should_discard_video else ""} with mpv: {target!r}')
      Player(app).run(target, bookmark, (site, target), should_resume)
    else:
      app.log.info(f'Opening {target!r}')
      subprocess.run(['xdg-open', target])

def deorphan_downloads(app):
  app.load_config()
  bookmarks = app.read_bookmarks()

  downloads.deorphan(bookmarks, app)

def list_unfinished(app):
  app.load_config()
  app.load_unfinished_bookmarks()

  if not app.unfinished_bookmarks:
    app.log.info('No unfinished bookmarks found')

  for i, entry in enumerate(app.unfinished_bookmarks.values()):
    print(f'{i + 1}. {entry["bookmark"]} at {format_playback_state(entry)}')

def finish(app):
  app.load_config()
  app.load_unfinished_bookmarks()

  result = list(app.unfinished_bookmarks.values())
  if not result:
    app.log.info('No unfinished bookmarks found')
    return

  for i, entry in enumerate(result):
    print(f'{i + 1}. {entry["bookmark"]} at {format_playback_state(entry)}')

  choice = input_choice(len(result))
  if choice is not None:
    app.save_in_history({'type': 'finish', 'bookmark': result[choice]['bookmark']})

def finish_all(app):
  app.load_config()
  app.load_unfinished_bookmarks()

  result = app.unfinished_bookmarks.values()
  if not result:
    app.log.info('No unfinished bookmarks found')
  elif input_question(f'Are you sure you want to finish all {len(result)} unfinished bookmarks?'):
    app.save_in_history([{'type': 'finish', 'bookmark': entry['bookmark']} for entry in result])

def print_(app):
  app.load_config()
  bookmarks = app.read_bookmarks()

  print(bookmarks.root.__str__(log=app.log), end='')

def print_diff(app):
  app.load_config()
  bookmarks = app.read_bookmarks()

  try:
    app.log.info('Running git diff')
    subprocess.run(['git', 'diff', '--no-index', os.path.expanduser(app.cfg.bookmarks), '-'], input=bookmarks.root.__str__(log=app.log).encode())
  except FileNotFoundError:
    app.log.info('Git is not installed, running regular diff')
    subprocess.run(['diff', os.path.expanduser(app.cfg.bookmarks), '-'], input=bookmarks.root.__str__(log=app.log).encode())

def print_inplace(app):
  app.load_config()
  bookmarks = app.read_bookmarks()

  with open(os.path.expanduser(app.cfg.bookmarks), 'r+') as file:
    file.write(bookmarks.root.__str__(log=app.log))
    file.truncate()
  app.log.info(f'Saved reformatted bookmarks in {app.cfg.bookmarks!r}')

def music_ranking(app):
  app.load_config()
  bookmarks = app.read_bookmarks()
  app.load_history()

  try:
    criterion = music.Criterion(app.cfg.criterion)
  except ValueError:
    app.log.error(f'Not a criterion: {app.cfg.criterion!r}')
  year = app.cfg.year
  try:
    format = music.Format(app.cfg.format)
  except ValueError:
    app.log.error(f'Not a format: {app.cfg.format!r}')
  limit = app.cfg.limit if format != music.Format.Html else None
  try:
    category = music.Category(app.cfg.category)
  except ValueError:
    app.log.error(f'Not a category: {app.cfg.category!r}')

  ranking = music.ranking(bookmarks, app.history, criterion, year, limit, category, format, app=app)

  if format == music.Format.Html:
    path = os.path.join(os.path.expanduser(app.cfg.cache), 'music_ranking.html')
    os.makedirs(os.path.dirname(path), exist_ok=True)
    with open(path, 'w') as file:
      file.write(ranking)

    app.log.info('Opening the music ranking')
    subprocess.run(['xdg-open', path])
  else:
    print(ranking, end='')

def music_activity(app):
  app.load_config()
  bookmarks = app.read_bookmarks()
  app.load_history()

  try:
    criterion = music.Criterion(app.cfg.criterion)
  except ValueError:
    app.log.error(f'Not a criterion: {app.cfg.criterion!r}')
  try:
    timescale = music.Timescale(app.cfg.timescale)
  except ValueError:
    app.log.error(f'Not a timescale: {app.cfg.timescale!r}')
  limit = app.cfg.limit
  year = app.cfg.year if timescale != music.Timescale.Months or limit is None else None

  print(music.activity(bookmarks, app.history, criterion, year, limit, timescale, app=app), end='')
