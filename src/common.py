# bm - The bookmarks.md manager
# Copyright (C) 2022-2023, 2025 Karol "digitcrusher" Łacina
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
from contextlib import contextmanager
from dataclasses import dataclass
from enum import Enum
from threading import Lock
from traceback import format_exception
from unicodedata import combining, normalize
from urllib.parse import urlparse

fixes = {'¡':'!','¢':'c','£':'L','¤':'o','¥':'Y','¦':'|','§':'S','©':'C','«':'<<','®':'R','°':'o','±':'+','¶':'P','·':'.','»':'>>','¿':'?','Æ':'AE','Ð':'D','×':'x','Ø':'O','Þ':'P','ß':'B','æ':'ae','ð':'d','÷':'-','ø':'o','þ':'p','Đ':'D','đ':'d','Ħ':'H','ħ':'h','ı':'i','ĸ':'K','Ł':'L','ł':'l','Ŋ':'N','ŋ':'n','Œ':'OE','œ':'oe','Ŧ':'T','ŧ':'t','ƀ':'b','Ɓ':'B','Ƃ':'b','ƃ':'b','Ƅ':'b','ƅ':'b','Ɔ':'C','Ƈ':'C','ƈ':'c','Ɖ':'D','Ɗ':'D','Ƌ':'d','ƌ':'d','ƍ':'o','Ǝ':'E','Ə':'e','Ɛ':'E','Ƒ':'F','ƒ':'f','Ɠ':'G','Ɣ':'Y','ƕ':'h','Ɨ':'I','Ƙ':'K','ƙ':'k','ƚ':'l','Ɯ':'W','Ɲ':'N','ƞ':'n','Ɵ':'O','Ƥ':'P','ƥ':'p','Ʀ':'R','Ƨ':'S','ƨ':'s','Ʃ':'E','ƪ':'l','ƫ':'t','Ƭ':'T','ƭ':'t','Ʈ':'T','Ʊ':'U','Ʋ':'U','Ƴ':'Y','ƴ':'y','Ƶ':'Z','ƶ':'z','Ʒ':'3','Ƹ':'E','ƹ':'E','ƺ':'3','ƻ':'2','Ƽ':'5','ƽ':'5','ƿ':'p','ǀ':'|','ǁ':'||','ǂ':'|','ǃ':'!','ǝ':'e','Ǥ':'G','ǥ':'g','Ƕ':'H','Ƿ':'p','Ȝ':'3','ȝ':'3','Ƞ':'n','ȡ':'d','Ȥ':'Z','ȥ':'z','ȴ':'l','ȵ':'n','ȶ':'t','ȷ':'j','ȸ':'db','ȹ':'qp','Ⱥ':'A','Ȼ':'C','ȼ':'c','Ƚ':'L','Ⱦ':'T','ȿ':'s','ɀ':'z','Ɂ':'?','ɂ':'?','Ƀ':'B','Ʉ':'U','Ʌ':'A','Ɇ':'E','ɇ':'e','Ɉ':'J','ɉ':'j','Ɋ':'q','ɋ':'q','Ɍ':'R','ɍ':'r','Ɏ':'Y','ɏ':'y','ɐ':'e','ɑ':'a','ɒ':'a','ɓ':'b','ɔ':'c','ɕ':'c','ɖ':'d','ɗ':'d','ɘ':'e','ə':'e','ɚ':'e','ɛ':'E','ɜ':'3','ɝ':'3','ɞ':'B','ɟ':'j','ɠ':'g','ɡ':'g','ɢ':'G','ɣ':'y','ɤ':'y','ɥ':'u','ɦ':'h','ɧ':'h','ɨ':'i','ɪ':'I','ɫ':'l','ɬ':'l','ɭ':'l','ɮ':'l3','ɯ':'w','ɰ':'w','ɱ':'m','ɲ':'n','ɳ':'n','ɴ':'N','ɵ':'o','ɶ':'oe','ɸ':'o','ɹ':'r','ɺ':'r','ɻ':'r','ɼ':'r','ɽ':'r','ɾ':'r','ɿ':'r','ʀ':'R','ʁ':'R','ʂ':'s','ʃ':'S','ʄ':'f','ʅ':'S','ʆ':'S','ʇ':'t','ʈ':'t','ʉ':'u','ʊ':'u','ʋ':'u','ʌ':'A','ʍ':'M','ʏ':'Y','ʐ':'z','ʑ':'z','ʒ':'3','ʓ':'3','ʔ':'?','ʕ':'?','ʖ':'?','ʗ':'C','ʘ':'O','ʙ':'B','ʚ':'B','ʛ':'G','ʜ':'H','ʝ':'j','ʞ':'k','ʟ':'L','ʠ':'q','ʡ':'?','ʢ':'?','ʣ':'dz','ʤ':'d3','ʥ':'dz','ʦ':'ts','ʧ':'tS','ʨ':'tc','ʩ':'fn','ʪ':'ls','ʫ':'lz','ʬ':'ww','ʮ':'u','ʯ':'u','ʹ':"'",'ʺ':"''",'ʻ':"'",'ʼ':"'",'ʽ':"'",'ʾ':"'",'ʿ':"'",'ˀ':'?','ˁ':'?','˂':'<','˃':'>','˄':'^','˅':'v','ˆ':'^','ˇ':'v','ˈ':"'",'ˉ':'-','ˊ':"'",'ˋ':'`','ˌ':',','ˍ':'_','ˎ':',','ˏ':',','ː':':','ˑ':'.','˒':'.','˖':'+','˗':'-','˟':'x','˦':'|','˧':'|','˨':'|','˪':'L','ˬ':'v','˭':'=','ˮ':'"','˯':'v','˰':'^','˱':'<','˲':'>','˳':'.','˴':'`','˵':'``','˶':"''",'˷':'~','˸':':','˽':'_','˾':'_','˿':'<-','Ͱ':'|','ͱ':'|','Ͳ':'T','ͳ':'T','͵':',','Ͷ':'N','ͷ':'N','ͻ':'c','ͼ':'c','ͽ':'c','Ϳ':'J','Α':'A','Β':'B','Γ':'r','Ε':'E','Ζ':'Z','Η':'H','Θ':'O','Ι':'I','Κ':'K','Λ':'A','Μ':'M','Ν':'N','Ο':'O','Π':'n','Ρ':'P','Σ':'E','Τ':'T','Υ':'Y','Φ':'o','Χ':'X','Ω':'O','α':'a','β':'B','γ':'y','ε':'E','ζ':'Z','η':'n','θ':'O','κ':'K','μ':'u','ν':'v','ξ':'E','ο':'o','π':'n','ρ':'p','ς':'c','σ':'o','τ':'t','υ':'u','φ':'o','χ':'X','ω':'w','Ϗ':'K','ϗ':'x','Ϙ':'O','ϙ':'o','Ϛ':'C','ϛ':'c','Ϝ':'F','ϝ':'f','ϟ':'S','Ϣ':'W','ϣ':'w','Ϥ':'q','ϥ':'q','Ϧ':'h','ϧ':'c','Ϩ':'2','ϩ':'2','Ϭ':'6','ϭ':'6','Ϯ':'I','ϯ':'f','ϳ':'j','϶':'E','Ϸ':'P','ϸ':'p','Ϻ':'M','ϻ':'M','ϼ':'p','Ͻ':'C','Ͼ':'C','Ͽ':'C','Ꭰ':'D','Ꭱ':'R','Ꭲ':'T','Ꭴ':'O','Ꭵ':'i','Ꭶ':'S','Ꭷ':'U','Ꭸ':'I','Ꭹ':'y','Ꭺ':'A','Ꭻ':'J','Ꭼ':'E','Ꭽ':'4','Ꭾ':'?','Ꭿ':'A','Ꮁ':'r','Ꮃ':'W','Ꮄ':'d','Ꮅ':'p','Ꮆ':'G','Ꮇ':'M','Ꮉ':'r','Ꮊ':'O','Ꮋ':'H','Ꮌ':'c','Ꮍ':'y','Ꮎ':'O','Ꮏ':'t','Ꮐ':'G','Ꮑ':'A','Ꮒ':'h','Ꮓ':'Z','Ꮕ':'O','Ꮖ':'I','Ꮙ':'V','Ꮛ':'E','Ꮜ':'U','Ꮞ':'4','Ꮟ':'b','Ꮠ':'I','Ꮡ':'r','Ꮢ':'R','Ꮣ':'l','Ꮤ':'W','Ꮥ':'S','Ꮦ':'b','Ꮩ':'V','Ꮪ':'S','Ꮭ':'L','Ꮮ':'L','Ꮯ':'C','Ꮲ':'P','Ꮳ':'C','Ꮴ':'V','Ꮵ':'h','Ꮶ':'K','Ꮷ':'d','Ꮸ':'C','Ꮹ':'G','Ꮺ':'U','Ꮻ':'O','Ꮼ':'U','Ꮽ':'9','Ꮾ':'6','Ᏸ':'B','Ᏺ':'h','Ᏻ':'G','Ᏼ':'B','Ᏽ':'G','ᏸ':'B','ᏺ':'h','ᏻ':'G','ᏼ':'B','ᏽ':'G','ᴀ':'A','ᴁ':'AE','ᴃ':'B','ᴄ':'c','ᴅ':'D','ᴆ':'D','ᴇ':'E','ᴈ':'3','ᴉ':'i','ᴊ':'j','ᴋ':'K','ᴌ':'L','ᴍ':'M','ᴎ':'N','ᴏ':'o','ᴐ':'c','ᴑ':'o','ᴒ':'c','ᴓ':'o','ᴔ':'eo','ᴘ':'P','ᴙ':'R','ᴚ':'R','ᴛ':'T','ᴜ':'U','ᴝ':'u','ᴞ':'u','ᴟ':'m','ᴠ':'v','ᴡ':'w','ᴢ':'z','ᴣ':'3','ᴤ':'S','ᴦ':'r','ᴧ':'A','ᴨ':'n','ᴩ':'P','ᴫ':'n','ᴯ':'B','ᴻ':'N','ᵎ':'i','ᵫ':'ue','ᵬ':'b','ᵭ':'d','ᵮ':'f','ᵯ':'m','ᵰ':'n','ᵱ':'p','ᵲ':'r','ᵳ':'f','ᵴ':'s','ᵵ':'t','ᵶ':'z','ᵷ':'g','ᵺ':'th','ᵻ':'I','ᵼ':'t','ᵽ':'p','ᵾ':'U','ᵿ':'u','ᶀ':'b','ᶁ':'d','ᶂ':'f','ᶃ':'g','ᶄ':'k','ᶅ':'l','ᶆ':'m','ᶇ':'n','ᶈ':'p','ᶉ':'r','ᶊ':'s','ᶋ':'S','ᶌ':'v','ᶍ':'x','ᶎ':'z','ᶏ':'a','ᶐ':'a','ᶑ':'d','ᶒ':'e','ᶓ':'E','ᶔ':'3','ᶕ':'e','ᶖ':'i','ᶗ':'c','ᶘ':'S','ᶙ':'u','ᶚ':'3','ẜ':'f','ẝ':'f','ẞ':'B','Ỻ':'IL','ỻ':'H','Ỽ':'6','ỽ':'6','Ỿ':'y','ỿ':'y','‐':'-','‒':'-','–':'-','—':'-','―':'-','‖':'||','‘':"'",'’':"'",'‚':"'",'‛':"'",'“':'"','”':'"','„':'"','‟':'"','•':'.','‣':'>','‧':'.','‰':'%o','‱':'%oo','′':"'",'‵':"'",'‸':'^','‹':'<','›':'>','※':'x','‽':'?!','⁃':'-','⁄':'/','⁅':'[','⁆':']','⁊':'7','⁋':'P','⁍':'D','⁎':'*','⁏':';','⁒':'%','⁓':'~','⁕':'*','⁚':':','⁜':'+','⁞':'|','₠':'CE','₡':'C','₢':'Cr','₣':'F','₤':'L','₥':'m','₦':'N','₧':'Pt','₩':'W','₫':'d','€':'E','₭':'K','₮':'T','₯':'Dp','₱':'P','₲':'G','₳':'A','₴':'S','₵':'C','₶':'H','₷':'S','₸':'T','₹':'R','₺':'L','₼':'m','₽':'P','₿':'B','⃀':'c','℄':'CL','℈':'E','℔':'b','℗':'P','℘':'P','℟':'R','℣':'V','℥':'3','℧':'U','℮':'e','Ⅎ':'F','℺':'Q','⅁':'G','⅄':'Y','⅊':'PL','⅋':'&','⅌':'P','⅍':'A/S','ⅎ':'F','ↁ':'D','Ↄ':'C','ↄ':'c','ↅ':'G','ↆ':'v','ↇ':'D','↊':'2','↋':'3','␀':'NUL','␁':'SOH','␂':'STX','␃':'ETX','␄':'EOT','␅':'ENQ','␆':'ACK','␇':'BEL','␈':'BS','␉':'HT','␊':'LF','␋':'VT','␌':'FF','␍':'CR','␎':'SS','␏':'SI','␐':'DLE','␑':'DC1','␒':'DC2','␓':'DC3','␔':'DC4','␕':'NAK','␖':'SYN','␗':'ETB','␘':'CAN','␙':'EM','␚':'SUB','␛':'ESC','␜':'FS','␝':'GS','␞':'RS','␟':'US','␠':'SP','␡':'DEL','␢':'b','␣':'_','␤':'NL','␥':'///','␦':'?','⓫':'11','⓬':'12','⓭':'13','⓮':'14','⓯':'15','⓰':'16','⓱':'17','⓲':'18','⓳':'19','⓴':'20','⓵':'1','⓶':'2','⓷':'3','⓸':'4','⓹':'5','⓺':'6','⓻':'7','⓼':'8','⓽':'9','⓾':'10','⓿':'0','Ⱡ':'L','ⱡ':'l','Ɫ':'L','Ᵽ':'P','Ɽ':'R','ⱥ':'a','ⱦ':'t','Ⱨ':'H','ⱨ':'h','Ⱪ':'K','ⱪ':'k','Ⱬ':'Z','ⱬ':'z','Ɑ':'a','Ɱ':'M','Ɐ':'A','Ɒ':'a','ⱱ':'v','Ⱳ':'W','ⱳ':'w','ⱴ':'v','Ⱶ':'I','ⱶ':'I','ⱷ':'o','ⱸ':'e','ⱹ':'r','ⱺ':'o','ⱻ':'E','Ȿ':'S','Ɀ':'Z','⸁':'F','⸆':'T','⸇':'T','⸈':'s','⸉':'s','⸊':'s','⸌':'\\','⸍':'/','⸏':'_','⸐':'_','⸑':'_','⸒':',','⸓':'/','⸖':'>','⸗':'//','⸘':'?!','⸚':'-','⸛':'~','⸜':'\\','⸝':'/','⸞':'~','⸟':'~','⸠':'|','⸡':'|','⸦':'c','⸨':'((','⸩':'))','⸮':'?','⸰':'o','⸱':'.','⸲':',','⸳':'.','⸴':',','⸵':';','⸺':'-','⸻':'-','⸼':'x','⸽':'|','⸿':'P','⹀':'=','⹁':',','⹂':'"','⹃':'-','⹄':':','⹉':':','⹊':'/','⹌':'?','⹎':':','⹏':'//','⹒':'7','⹕':'[','⹖':']','⹗':'[','⹘':']','Ꜣ':'3','ꜣ':'3','Ꜧ':'H','ꜧ':'h','Ꜩ':'T3','ꜩ':'t3','Ꜫ':'E','ꜫ':'E','Ꜭ':'4','ꜭ':'4','Ꜯ':'4','ꜯ':'4','ꜰ':'F','ꜱ':'s','Ꜳ':'AA','ꜳ':'aa','Ꜵ':'AO','ꜵ':'ao','Ꜷ':'AU','ꜷ':'au','Ꜹ':'AV','ꜹ':'av','Ꜻ':'AV','ꜻ':'av','Ꜽ':'AY','ꜽ':'ay','Ꜿ':'C','ꜿ':'c','Ꝁ':'K','ꝁ':'k','Ꝃ':'K','ꝃ':'k','Ꝅ':'K','ꝅ':'k','Ꝇ':'L','ꝇ':'l','Ꝉ':'L','ꝉ':'l','Ꝋ':'O','ꝋ':'o','Ꝍ':'O','ꝍ':'o','Ꝏ':'OO','ꝏ':'oo','Ꝑ':'P','ꝑ':'p','Ꝓ':'P','ꝓ':'p','Ꝕ':'P','ꝕ':'p','Ꝗ':'Q','ꝗ':'q','Ꝙ':'Q','ꝙ':'q','Ꝛ':'R','ꝛ':'r','Ꝟ':'V','ꝟ':'v','Ꝡ':'W','ꝡ':'w','Ꝣ':'3','ꝣ':'3','Ꝥ':'P','ꝥ':'p','Ꝧ':'P','ꝧ':'p','Ꝫ':'3','ꝫ':'3','Ꝭ':'f','Ꝯ':'9','ꝯ':'9','ꝱ':'d','ꝲ':'l','ꝳ':'m','ꝴ':'n','ꝵ':'r','ꝶ':'R','ꝷ':'t','ꝸ':'&','Ꝺ':'O','ꝺ':'d','Ꝼ':'f','ꝼ':'f','Ᵹ':'d','Ꝿ':'o','ꝿ':'o','Ꞁ':'r','ꞁ':'|','Ꞃ':'r','ꞃ':'r','Ꞅ':'r','ꞅ':'r','Ꞇ':'C','ꞇ':'c','꞉':':','꞊':'=','Ꞌ':"'",'ꞌ':"'",'Ɥ':'4','ꞎ':'l','ꞏ':'.','Ꞑ':'N','ꞑ':'n','Ꞓ':'C','ꞓ':'c','ꞔ':'c','ꞕ':'h','Ꞗ':'B','ꞗ':'b','Ꞙ':'F','ꞙ':'f','Ꞛ':'B','ꞛ':'B','Ꞝ':'B','ꞝ':'B','Ꞟ':'R','ꞟ':'R','Ꞡ':'G','ꞡ':'g','Ꞣ':'K','ꞣ':'k','Ꞥ':'N','ꞥ':'n','Ꞧ':'R','ꞧ':'r','Ꞩ':'S','ꞩ':'s','Ɦ':'H','Ɜ':'3','Ɡ':'g','Ɬ':'L','Ɪ':'I','ꞯ':'Q','Ʞ':'K','Ʇ':'T','Ʝ':'J','Ꭓ':'X','Ꞵ':'B','ꞵ':'B','Ꞷ':'W','ꞷ':'w','Ꞹ':'U','ꞹ':'u','Ꞻ':'A','ꞻ':'a','Ꞽ':'I','ꞽ':'i','Ꞿ':'U','ꞿ':'u','Ꟁ':'O','ꟁ':'o','Ꟃ':'VB','ꟃ':'VB','Ꞔ':'C','Ʂ':'S','Ᶎ':'Z','Ꟈ':'D','ꟈ':'d','Ꟊ':'S','ꟊ':'s','Ꟑ':'g','ꟑ':'g','ꟓ':'B','ꟕ':'B','Ꟗ':'B','ꟗ':'B','Ꟙ':'d','ꟙ':'d','Ꟶ':'I','ꟶ':'I','ꟷ':'-','ꟺ':'w','ꟻ':'F','ꟼ':'P','ꟽ':'W','ꟾ':'I','ꬰ':'a','ꬱ':'ae','ꬲ':'e','ꬳ':'e','ꬴ':'e','ꬵ':'f','ꬶ':'g','ꬷ':'l','ꬸ':'l','ꬹ':'l','ꬺ':'m','ꬻ':'n','ꬼ':'n','ꬽ':'o','ꬾ':'o','ꬿ':'c','ꭀ':'oe','ꭁ':'eo','ꭂ':'eo','ꭃ':'uo','ꭄ':'uo','ꭆ':'R','ꭇ':'r','ꭈ':'r','ꭉ':'r','ꭊ':'r','ꭋ':'r','ꭌ':'b','ꭍ':'S','ꭎ':'u','ꭏ':'u','ꭐ':'w','ꭑ':'m','ꭒ':'u','ꭓ':'X','ꭔ':'X','ꭕ':'X','ꭖ':'x','ꭗ':'x','ꭘ':'x','ꭙ':'x','ꭚ':'y','ꭡ':'ie','ꭢ':'ce','ꭣ':'uo','ꭤ':'a','ꭥ':'o','ꭦ':'dz','ꭧ':'ts','ꭨ':'r','ꭰ':'D','ꭱ':'R','ꭲ':'T','ꭴ':'o','ꭵ':'i','ꭶ':'s','ꭷ':'o','ꭸ':'I','ꭹ':'y','ꭺ':'A','ꭻ':'J','ꭼ':'E','ꭽ':'ov','ꭾ':'?','ꭿ':'A','ꮁ':'r','ꮃ':'w','ꮄ':'d','ꮅ':'p','ꮆ':'G','ꮇ':'M','ꮉ':'r','ꮊ':'o','ꮋ':'H','ꮍ':'y','ꮎ':'o','ꮏ':'t','ꮐ':'G','ꮑ':'n','ꮒ':'h','ꮓ':'z','ꮔ':'q','ꮕ':'o','ꮖ':'I','ꮙ':'v','ꮚ':'w','ꮛ':'E','ꮜ':'U','ꮞ':'4','ꮟ':'b','ꮠ':'I','ꮡ':'r','ꮢ':'R','ꮣ':'b','ꮤ':'W','ꮥ':'s','ꮦ':'b','ꮧ':'r','ꮨ':'r','ꮩ':'v','ꮪ':'s','ꮭ':'L','ꮮ':'L','ꮯ':'c','ꮲ':'P','ꮳ':'c','ꮴ':'v','ꮵ':'h','ꮶ':'K','ꮷ':'d','ꮸ':'c','ꮹ':'G','ꮺ':'u','ꮻ':'o','ꮽ':'9','ꮾ':'6','ꮿ':'w'}
'''
# Code used for creating the lookup table above:
from unicodedata import combining, normalize
norm = lambda string: ''.join(c for c in normalize('NFKD', string) if combining(c) == 0)
gen_base = lambda a, b: {c: fixes.get(c, c) for c in map(chr, range(a, b + 1)) if c.isprintable() and norm(c) == c}
gen_missing_derived = lambda a, b: {k: v for k, v in {c: norm(c).translate(str.maketrans(fixes)) for c in map(chr, range(a, b + 1)) if c.isprintable() and norm(c) != c}.items() if not v.isascii()}
union = lambda dicts: dict(sum((list(i.items()) for i in dicts), start=[]))
ranges_of_interest = [
  (0x0080, 0x00FF), # Latin-1 Supplement
  (0x0100, 0x017F), # Latin Extended-A
  (0x0180, 0x024F), # Latin Extended-B
  (0x0250, 0x02AF), # IPA Extensions
  (0x02B0, 0x02FF), # Spacing Modifier Letters
  (0x0370, 0x03FF), # Greek and Coptic
  (0x13A0, 0x13FF), # Cherokee
  (0x1D00, 0x1D7F), # Phonetic Extensions
  (0x1D80, 0x1DBF), # Phonetic Extensions Supplement
  (0x1E00, 0x1EFF), # Latin Extended Additional
  (0x1F00, 0x1FFF), # Greek Extended
  (0x2000, 0x206F), # General Punctuation
  (0x20A0, 0x20CF), # Currency Symbols
  (0x2100, 0x214F), # Letterlike Symbols
  (0x2150, 0x218F), # Number Forms
  (0x2400, 0x243F), # Control Pictures
  (0x2460, 0x24FF), # Enclosed Alphanumerics
  (0x2C60, 0x2C7F), # Latin Extended-C
  (0x2E00, 0x2E7F), # Supplemental Punctuation
  (0xA720, 0xA7FF), # Latin Extended-D
  (0xAB30, 0xAB6F), # Latin Extended-E
  (0xAB70, 0xABBF), # Cherokee Supplement
]
'''
assert all(v.isascii() for v in fixes.values())
fixes = str.maketrans(fixes)
def ascii_mimic(string):
  return ''.join(c for c in normalize('NFKD', string) if combining(c) == 0).translate(fixes)

def slugify(string):
  result = ''
  for c in ascii_mimic(string.casefold()):
    if c.isalnum():
      result += c
    elif c not in "'`" and not result.endswith('_'):
      result += '_'
  return result.strip('_')

def format_duration(seconds):
  units = [
    ('h', 60 * 60, 1),
    ('m', 60, None),
    ('s', 1, None),
  ]
  for sym, val, prec in units:
    if seconds < val and (sym, val, prec) != units[-1]:
      continue
    return f'{round(seconds / val, prec)}{sym}'

def is_url(string):
  url = urlparse(string)
  return url.scheme and url.netloc

def fuzzy_match(needle, haystack):
  score = 0
  is_matched = [False] * len(haystack)

  needle = ascii_mimic(needle).lower()
  j = 0
  run = 0
  for i, c in enumerate(haystack):
    c = ascii_mimic(c).lower()
    if needle.startswith(c, j):
      run += 1
      score += run
      is_matched[i] = True
      j += len(c)
      if j >= len(needle):
        break
    else:
      run = 0

  return None if j < len(needle) else (score, is_matched)

class Logger:
  class Error(Exception):
    pass

  class Level(Enum):
    Error = 'error'
    Warn = 'warn'
    Info = 'info'
    Debug = 'debug'

  is_verbose: bool
  history: list[(Level, str)]

  def __init__(self, is_verbose=False):
    self.is_verbose = is_verbose
    self.history = []
    self.lock = Lock()
    self.line_num_stack = [None]

  def error(self, msg):
    self.handle_event(Logger.Level.Error, self.format_msg(msg))

  def warn(self, msg):
    self.handle_event(Logger.Level.Warn, self.format_msg(msg))

  def info(self, msg):
    self.handle_event(Logger.Level.Info, self.format_msg(msg))

  def debug(self, msg):
    self.handle_event(Logger.Level.Debug, self.format_msg(msg))

  def except_msg(self, exc, circumstances=None):
    result = 'Caught the following exception'
    if circumstances:
      result += ' while ' + circumstances
    result += ':\n'
    result += ''.join(format_exception(None, exc, exc.__traceback__))
    return result

  @contextmanager
  def line_num(self, val):
    self.line_num_stack.append(val)
    try:
      yield
    finally:
      self.line_num_stack.pop()

  def flush_into(self, other):
    with self.lock:
      for level, msg in self.history:
        other.handle_event(level, msg)
      self.history.clear()

  def handle_event(self, level, msg):
    with self.lock:
      self.history.append((level, msg))
      if level != Logger.Level.Debug or self.is_verbose:
        self.print(level, msg)
    if level == Logger.Level.Error:
      raise Logger.Error(msg)

  def print(self, level, msg):
    pass

  def format_msg(self, msg):
    if self.line_num_stack[-1] is not None:
      line_num = f' at line {self.line_num_stack[-1]}'
      a, b, c = msg.partition(':')
      a += line_num
      msg = a + b + c
    if not msg.endswith('\n'):
      msg += '\n'
    return msg

class StderrLogger(Logger):
  def print(self, level, msg):
    colors = {
      Logger.Level.Error: '31',
      Logger.Level.Warn: '33',
      Logger.Level.Info: '34',
      Logger.Level.Debug: '32',
    }
    msg = msg[0].lower() + msg[1:]
    sys.stderr.write(f'\x1b[{colors[level]};1m{level.value}:\x1b[0m {msg}')

@dataclass
class MusicMetadata:
  class Format(Enum):
    Artist = 'artist'
    Song = 'song'
    Album = 'album'
    ExtendedPlay = 'extended play'
    SongCompilation = 'song compilation'
    AlbumCompilation = 'album compilation'
    ExtendedPlayCompilation = 'extended play compilation'
    LiveSongCompilation = 'live song compilation'

    def __str__(self):
      return self.value

  author: str | None
  title: str
  format: Format | None

  @staticmethod
  def parse(title, *, log=Logger()):
    format = None
    title = title.rstrip()
    if ' (' in title and title.endswith(')'):
      title, _, format = title[:-1].rpartition(' (')
      format = format.strip()
      try:
        format = MusicMetadata.Format(format)
      except ValueError:
        log.warn(f'Unknown music format: {format!r}')
        format = None

    title = title.strip()
    if format == MusicMetadata.Format.Artist:
      author = title
    else:
      author = None
      if ' - ' in title:
        author, _, remainder = title.partition(' - ')
        if title.startswith('"') and title.endswith('"'):
          author = author[1:]
        else:
          title = remainder.lstrip()
        author = author.strip()

    return MusicMetadata(author, title, format)

class Observable:
  def __init__(self, default=None):
    self.default = default

  def __set_name__(self, type, name):
    self.name = name
    self.infect(type)

  def __set__(self, obj, val):
    old_val = self.__get__(obj)
    obj._observables[self.name] = val
    if old_val != val:
      try:
        obj.__all_observer__(self.name)
      except AttributeError:
        pass
      for callback in obj._observers.get(self.name, []):
        callback()

  def __get__(self, obj, type=None):
    return obj._observables.get(self.name, self.default)

  @staticmethod
  def infect(type):
    if hasattr(type, 'observe'):
      return

    original_init = type.__init__
    def __init__(self, *args, **kwargs):
      self._observables = {}
      self._observers = {}
      original_init(self, *args, **kwargs)

    def observe(self, name, callback):
      if not isinstance(name, str):
        raise TypeError('observable name must be string')
      if not isinstance(vars(type).get(name), Observable):
        raise AttributeError(f'{type.__name__!r} object has no observable {name!r}', name=name, obj=self)
      self._observers.setdefault(name, []).append(callback)

    def unobserve(self, callback):
      for i in self._observers.values():
        try:
          i.remove(callback)
        except ValueError:
          pass

    def notify_observers(self, name, *args):
      if not isinstance(name, str):
        raise TypeError('observable name must be string')
      if not isinstance(vars(type).get(name), Observable):
        raise AttributeError(f'{type.__name__!r} object has no observable {name!r}', name=name, obj=self)
      try:
        self.__all_observer__(name, *args)
      except AttributeError:
        pass
      for callback in self._observers.get(name, []):
        callback(*args)

    type.__init__ = __init__
    type.observe = observe
    type.unobserve = unobserve
    type.notify_observers = notify_observers

class Counter:
  def __init__(self, value=0):
    self.value = value
    self.lock = Lock()

  def increment(self):
    with self.lock:
      result = self.value
      self.value += 1
      return result

  def decrement(self):
    with self.lock:
      result = self.value
      self.value -= 1
      return result
