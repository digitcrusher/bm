# bm - The bookmarks.md manager
# Copyright (C) 2022-2023, 2025 Karol "digitcrusher" Łacina
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import json, os, yaml
from copy import deepcopy
from dataclasses import asdict, dataclass, field, fields, replace
from datetime import datetime
from threading import RLock, Thread
from timeit import default_timer

from common import Logger, slugify, StderrLogger
from model import Bookmarks, Category

@dataclass(kw_only=True)
class Options:
  dev: bool = False
  config: str = '~/.config/bm/config.yaml'
  save_defaults: bool = False

@dataclass(kw_only=True)
class Config:
  bookmarks: str | None = None
  history: str = '~/.config/bm/history.json'
  cache: str = '~/.cache/bm/'

  search_limit: int | None = 10
  download: bool = True
  ytdl_options: list[str] = field(default_factory=lambda: ['--embed-metadata', '--embed-chapters', '--embed-subs', '--embed-thumbnail', '--no-mtime'])
  sort_by: str | None = None
  sort_in_reverse: bool = False

  criterion: str = 'play-time'
  year: int | None = None
  limit: int | None = 10
  category: str = 'titles'
  format: str = 'html'
  timescale: str = 'months'

  @staticmethod
  def empty():
    result = Config()
    for field in fields(result):
      setattr(result, field.name, ...)
    return result

  @staticmethod
  def from_dict(dict):
    result = Config.empty()
    result.refine(dict)
    return result

  def to_dict(self):
    return asdict(self, dict_factory=lambda x: {key: val for key, val in x if val is not ...})

  def refine(self, other):
    if isinstance(other, Config):
      for field in fields(other):
        if getattr(self, field.name) is ...:
          setattr(self, field.name, deepcopy(getattr(other, field.name)))
    else:
      for key, val in other.items():
        if getattr(self, key) is ...:
          setattr(self, key, deepcopy(val))

class App:
  def __init__(self):
    self.log = StderrLogger()
    self.opt = Options()
    self.cfg = Config.empty()
    self.history = None
    self.unfinished_bookmarks = None
    self.history_lock = RLock()

    self.history_mtime = None
    self.history_thread = None

  def load_config(self):
    try:
      with open(os.path.expanduser(self.opt.config), 'r') as file:
        from_disk = yaml.safe_load(file)
        if not isinstance(from_disk, dict):
          self.log.error('Config is not a valid YAML dictionary')
        self.cfg.refine(from_disk)
    except FileNotFoundError:
      self.log.warn(f'Config not found: {self.opt.config!r}')
    except Exception as e:
      self.log.error(self.log.except_msg(e, 'parsing config'))

    self.cfg.refine(Config())

  def save_config(self):
    path = os.path.expanduser(self.opt.config)
    saved = replace(self.cfg)

    try:
      with open(path, 'r') as file:
        from_disk = yaml.safe_load(file)
        if not isinstance(from_disk, dict):
          self.log.error('Config is not a valid YAML dictionary')
        saved.refine(from_disk)
    except FileNotFoundError:
      pass
    except Exception as e:
      self.log.error(self.log.except_msg(e, 'parsing config'))

    if self.opt.save_defaults:
      saved.refine(Config())

    if os.path.dirname(path):
      os.makedirs(os.path.dirname(path), exist_ok=True)
    with open(path, 'w') as file:
      yaml.dump(saved.to_dict(), file, allow_unicode=True, sort_keys=False)
    self.log.info(f'Saved config in {self.opt.config!r}')

  def read_bookmarks(self):
    if self.cfg.bookmarks is None:
      self.log.error('No path to bookmarks set')
    try:
      with open(os.path.expanduser(self.cfg.bookmarks), 'r') as file:
        result = Bookmarks.parse(file.read(), log=self.log)
        if self.cfg.sort_by is not None:
          result.root.sort(Category.SortOrder(self.cfg.sort_by), self.cfg.sort_in_reverse, True)
        return result
    except FileNotFoundError:
      self.log.error(f'Bookmarks not found: {self.cfg.bookmarks!r}')

  def load_history(self):
    start = default_timer()
    try:
      with open(os.path.expanduser(self.cfg.history), 'r') as file:
        mtime = os.fstat(file.fileno()).st_mtime
        with self.history_lock:
          if mtime == self.history_mtime or (self.history_mtime is None and self.history is not None) or self.history_mtime is ...:
            return

          try:
            history = json.load(file)
            history.sort(key=lambda x: x['time'])
          except Exception as e:
            self.log.error(self.log.except_msg(e, 'parsing history'))

          self.history = history
          self.unfinished_bookmarks = None
          self.history_mtime = mtime

    except FileNotFoundError:
      with self.history_lock:
        if self.history is None:
          self.history = []
          self.unfinished_bookmarks = {}
          self.history_mtime = -1

    end = default_timer()
    self.log.debug(f'Loaded history in {end - start:.2f}s')

  def load_unfinished_bookmarks(self):
    self.load_history()
    with self.history_lock:
      if self.unfinished_bookmarks is None:
        self.unfinished_bookmarks = {}
        self.update_unfinished_bookmarks(self.history)

  def save_in_history(self, entries):
    if not isinstance(entries, list):
      entries = [entries]
    time = datetime.now().astimezone().isoformat()
    for i in range(len(entries)):
      assert 'type' in entries[i]
      if 'time' not in entries[i]:
        entries[i] = {'time': time} | entries[i]

    self.load_history()
    with self.history_lock:
      self.history += entries
      if self.unfinished_bookmarks is not None:
        self.update_unfinished_bookmarks(entries)
      self.history_mtime = None
      if self.history_thread is None:
        self.history_thread = Thread(target=self.history_saver)
        self.history_thread.start()

  def history_saver(self):
    while True:
      with self.history_lock:
        if self.history_mtime is not None or self.history is None:
          self.history_thread = None
          break

      try:
        start = default_timer()
        path = os.path.expanduser(self.cfg.history)
        if os.path.dirname(path):
          os.makedirs(os.path.dirname(path), exist_ok=True)
        try:
          os.rename(path, path + '.old')
        except FileNotFoundError:
          pass

        with open(path, 'w') as file:
          with self.history_lock:
            history = self.history.copy()
            self.history_mtime = ...

          json.dump(history, file, ensure_ascii=False, separators=(',', ':'))

          with self.history_lock:
            if self.history_mtime is ...:
              self.history_mtime = os.fstat(file.fileno()).st_mtime

        try:
          os.remove(path + '.old')
        except:
          pass
        end = default_timer()
        self.log.debug(f'Saved history in {end - start:.2f}s')
      except Exception as e:
        self.log.warn(self.log.except_msg(e, 'saving history'))

  def update_unfinished_bookmarks(self, entries):
    for entry in entries:
      if 'bookmark' in entry:
        slug = slugify(entry['bookmark'])
        if entry['type'] == 'play':
          self.unfinished_bookmarks[slug] = entry
        elif entry['type'] == 'finish':
          del self.unfinished_bookmarks[slug]
