# bm - The bookmarks.md manager
# Copyright (C) 2022-2023, 2025 Karol "digitcrusher" Łacina
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os, shutil, subprocess, sys
from dataclasses import dataclass

from common import is_url, slugify

def path_for(url, app):
  return os.path.join(os.path.expanduser(app.cfg.cache), 'downloads', slugify(url))

@dataclass
class YtdlError(Exception):
  log: str
  files: list[str]

def download_with_ytdl(url, bookmark, on_progress=None, *, app):
  app.log.info(f'Trying to download{" audio" if bookmark.should_discard_video else ""} with yt-dlp: {url!r}')

  dest = path_for(url, app)
  args = ['yt-dlp', url] + app.cfg.ytdl_options + [
    '-o', dest.replace('%', '%%') + '-%(playlist_index)d.%(ext)s',
    '-k', '--print', 'after_move:file %(filepath)s', '--no-quiet', # All this just to avoid redownloading the whole music playlist when an error occurs.
  ]
  if bookmark.should_discard_video:
    args += ['-x', '--no-embed-subs', '--no-embed-thumbnail']
  if on_progress is not None:
    args += [
      '--progress-template', 'progress %(info.playlist_autonumber-1|0)d %(info.n_entries|1)d %(progress.downloaded_bytes|0)d %(progress.total_bytes,progress.total_bytes_estimate|inf)s',
      '--print', 'start %(playlist_autonumber-1|0)d %(n_entries|1)d',
    ]

  os.makedirs(os.path.dirname(dest), exist_ok=True)
  ytdl = subprocess.Popen(args, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, text=True)

  try:
    files = []
    log = ''
    a = b = c = 0.0 # The total bytes estimate is usually very jittery, so we smooth it out.
    for line in ytdl.stdout:
      if line.startswith('file '):
        files.append(line.removeprefix('file ').removesuffix('\n'))

      elif line.startswith('progress '): # When a video is at 100%, this gets sent twice, which is just ideal for our median of three here.
        video, videoc, byte, bytec = map(float, line.split()[1:])
        a = b
        b = c
        c = (video + byte / bytec) / videoc
        on_progress(sorted((a, b, c))[1])

      elif line.startswith('start '):
        video, videoc = map(int, line.split()[1:])
        a = b = c = video / videoc
        on_progress(sorted((a, b, c))[1])

      else:
        log += line
        sys.stderr.write(line)

  except KeyboardInterrupt:
    app.log.info('Caught a keyboard interrupt')
    cleanup_download(url, app)
    raise

  if ytdl.wait() != 0:
    raise YtdlError(log, files)

  complete_download(url, files, app)

def complete_download(url, files, app):
  dest = path_for(url, app)
  if len(files) == 1:
    os.rename(files[0], dest)
  else:
    os.makedirs(dest, exist_ok=True)
    for file in files:
      number = file.rpartition('-')[2].partition('.')[0]
      os.rename(file, os.path.join(dest, number))

  cleanup_download(url, app)

def cleanup_download(url, app):
  dest = path_for(url, app)
  for file in os.listdir(os.path.dirname(dest)):
    if file.startswith(os.path.basename(dest) + '-'):
      os.remove(os.path.join(os.path.dirname(dest), file))

def deorphan(bookmarks, app):
  urls = set()
  def visit(bookmark):
    for _, target in bookmark.links:
      if is_url(target):
        urls.add(slugify(target))
  bookmarks.root.walk(visit)

  downloads = os.path.join(os.path.expanduser(app.cfg.cache), 'downloads')
  files = [os.path.join(downloads, file) for file in os.listdir(downloads) if file not in urls]
  if not files:
    app.log.info('No orphaned files found')

  for file in files:
    if os.path.isfile(file):
      os.remove(file)
    else:
      shutil.rmtree(file)
    app.log.info(f'Deleted {file!r}')
