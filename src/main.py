#!/usr/bin/env python3
#
# bm - The bookmarks.md manager
# Copyright (C) 2022-2023, 2025 Karol "digitcrusher" Łacina
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os, sys

import cli
from app import App
from common import Logger

# TODO: icon

help_msg = f'''\
Usage: {os.path.basename(sys.argv[0])} [<options>...] <operation> [...]

The bookmarks.md manager

Operations:
  h, help                    prints this help message
  c, configure               saves the command line options to the config file

  o, open <pattern>          searches for and opens a bookmark
  dd, deorphan-downloads     deletes orphaned files from downloads
  lu, list-unfinished        shows saved mpv playback state of unfinished
                             bookmarks
  f, finish                  forgets about an unfinished bookmark
  fa, finish-all             forgets about all unfinished bookmarks
  p, print                   prints the reformatted bookmarks.md
  pd, print-diff             diffs the old and reformatted bookmarks.md
  pi, print-inplace          reformats the bookmarks.md
  mr, music-ranking          generates a music ranking from the history
  ma, music-activity         generates a music activity chart from the history

  gui                        activates the GTK 4 frontend
  wd, write-desktop [<dir>]  writes the GUI's .desktop file to the given
                             directory or ~/.local/share/applications/

Options:
  -c, --config <file>              sets the path to the config file
  --save-defaults                  includes the defaults when saving the config
  -b, --bookmarks <file>           sets the path to the bookmarks.md
  -h, --history <file>             sets the path to the history file
  --cache <directory>              sets the path to the cache directory
  -v, --verbose                    prints debug messages

  --search-limit <count>|inf       limits the number of results in searches
  --download                       caches bookmarks (default)
  --no-download                    avoids caching bookmarks
  --ytdl-options [<options>...] ;  sets the additional options passed to yt-dlp
  --sort-by none|name|added        sorts bookmarks by the given order
  --sort-in-reverse                sorts bookmarks in reverse order
  --no-sort-in-reverse             sorts bookmarks in normal order (default)

  --criterion opens|play-time      scores music by the given criterion
  --year <year>|all                narrows history down to the given year
  --limit <count>|inf              limits the number of results in music stats
  --category titles|authors        ranks the given music category
  --format html|text               generates music rankings in the given format
  --timescale days|weeks|months    charts music activity in the given timescale'''

def main(args, app):
  positional = []

  i = 0
  while i < len(args):
    if args[i] == '--dev':
      app.opt.dev = True

    elif args[i] in {'-c', '--config'}:
      try:
        i += 1
        app.opt.config = args[i]
      except IndexError:
        app.log.error(f'Expected a path to config after {args[i - 1]!r}')

    elif args[i] == '--save-defaults':
      app.opt.save_defaults = True

    elif args[i] in {'-b', '--bookmarks'}:
      try:
        i += 1
        app.cfg.bookmarks = args[i]
      except IndexError:
        app.log.error(f'Expected a path to bookmarks after {args[i - 1]!r}')

    elif args[i] in {'-h', '--history'}:
      try:
        i += 1
        app.cfg.history = args[i]
      except IndexError:
        app.log.error(f'Expected a path to history after {args[i - 1]!r}')

    elif args[i] == '--cache':
      try:
        i += 1
        app.cfg.cache = args[i]
      except IndexError:
        app.log.error(f'Expected a path to cache after {args[i - 1]!r}')

    elif args[i] in {'-v', '--verbose'}:
      app.log.is_verbose = True

    elif args[i] == '--search-limit':
      try:
        i += 1
        try:
          app.cfg.search_limit = None if args[i] == 'inf' else int(args[i])
        except ValueError:
          app.log.error(f'Not an integer: {args[i]!r}')
      except IndexError:
        app.log.error(f'Expected a limit after {args[i - 1]!r}')

    elif args[i] == '--download':
      app.cfg.download = True

    elif args[i] == '--no-download':
      app.cfg.download = False

    elif args[i] == '--ytdl-options':
      try:
        i += 1
        app.cfg.ytdl_options = []
        while args[i] != ';':
          app.cfg.ytdl_options.append(args[i])
          i += 1
      except IndexError:
        app.log.error(f'Expected a semicolon at the end of the yt-dlp options')

    elif args[i] == '--sort-by':
      try:
        i += 1
        app.cfg.sort_by = None if args[i] == 'none' else args[i]
      except IndexError:
        app.log.error(f'Expected an order after {args[i - 1]!r}')

    elif args[i] == '--sort-in-reverse':
      app.cfg.sort_in_reverse = True

    elif args[i] == '--no-sort-in-reverse':
      app.cfg.sort_in_reverse = False

    elif args[i] == '--criterion':
      try:
        i += 1
        app.cfg.criterion = args[i]
      except IndexError:
        app.log.error(f'Expected a criterion after {args[i - 1]!r}')

    elif args[i] == '--year':
      try:
        i += 1
        try:
          app.cfg.year = None if args[i] == 'all' else int(args[i])
        except ValueError:
          app.log.error(f'Not an integer: {args[i]!r}')
      except IndexError:
        app.log.error(f'Expected a year after {args[i - 1]!r}')

    elif args[i] == '--category':
      try:
        i += 1
        app.cfg.category = args[i]
      except IndexError:
        app.log.error(f'Expected a category after {args[i - 1]!r}')

    elif args[i] == '--format':
      try:
        i += 1
        app.cfg.format = args[i]
      except IndexError:
        app.log.error(f'Expected a format after {args[i - 1]!r}')

    elif args[i] == '--limit':
      try:
        i += 1
        try:
          app.cfg.limit = None if args[i] == 'inf' else int(args[i])
        except ValueError:
          app.log.error(f'Not an integer: {args[i]!r}')
      except IndexError:
        app.log.error(f'Expected a limit after {args[i - 1]!r}')

    elif args[i] == '--timescale':
      try:
        i += 1
        app.cfg.timescale = args[i]
      except IndexError:
        app.log.error(f'Expected a timescale after {args[i - 1]!r}')

    elif args[i] == '--':
      positional += args[i + 1:]
      break

    elif args[i].startswith('-') and len(args[i]) >= 2:
      app.log.error(f'Unknown option: {args[i]!r}')

    else:
      positional.append(args[i])
    i += 1

  try:
    op = positional.pop(0)
  except IndexError:
    op = 'help'

  def ensure_no_positional():
    if positional:
      app.log.error(f'Operation {op!r} does not expect any positional arguments')

  if op in {'h', 'help'}:
    ensure_no_positional()
    print(help_msg)
  elif op in {'c', 'configure'}:
    ensure_no_positional()
    cli.configure(app)
  elif op in {'o', 'open'}:
    cli.open_(' '.join(positional), app)
  elif op in {'dd', 'deorphan-downloads'}:
    ensure_no_positional()
    cli.deorphan_downloads(app)
  elif op in {'lu', 'list-unfinished'}:
    ensure_no_positional()
    cli.list_unfinished(app)
  elif op in {'f', 'finish'}:
    ensure_no_positional()
    cli.finish(app)
  elif op in {'fa', 'finish-all'}:
    ensure_no_positional()
    cli.finish_all(app)
  elif op in {'p', 'print'}:
    ensure_no_positional()
    cli.print_(app)
  elif op in {'pd', 'print-diff'}:
    ensure_no_positional()
    cli.print_diff(app)
  elif op in {'pi', 'print-inplace'}:
    ensure_no_positional()
    cli.print_inplace(app)
  elif op in {'mr', 'music-ranking'}:
    ensure_no_positional()
    cli.music_ranking(app)
  elif op in {'ma', 'music-activity'}:
    ensure_no_positional()
    cli.music_activity(app)
  elif op == 'gui':
    import gui
    ensure_no_positional()
    gui.GuiApp(app).run()
  elif op in {'wd', 'write-desktop'}:
    import gui
    if len(positional) > 1:
      app.log.error('Too many positional arguments')
    gui.write_desktop(*positional, app=app)
  else:
    app.log.error(f'Unknown operation: {op!r}')

if __name__ == '__main__':
  try:
    main(sys.argv[1:], App())
  except Logger.Error:
    sys.exit(123)
