# bm - The bookmarks.md manager
# Copyright (C) 2022-2023, 2025 Karol "digitcrusher" Łacina
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import json, socket, subprocess
from concurrent.futures import Future
from copy import deepcopy
from datetime import datetime
from enum import auto, Enum
from threading import main_thread

from common import Counter, Logger, Observable, slugify

def is_file_playable(path):
  return subprocess.run(['ffprobe', path], stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL).returncode == 0

def format_playback_time(seconds):
  sign = '-' if seconds < 0 else ''
  seconds = abs(int(seconds))
  h = seconds // 60 // 60
  m = seconds // 60 % 60
  s = seconds % 60
  if h > 0:
    return f'{sign}{h}:{m:02}:{s:02}'
  else:
    return f'{sign}{m}:{s:02}'

# This is a wrapper over mpv's JSON IPC that filters its stupidities out and presents the data in a friendly and usable way.
class Mpv:
  # These properties are updated in a random order and independent of whatever else is going on.
  is_seeking: bool                  = Observable(True)
  is_paused: bool                   = Observable(False)
  speed: float                      = Observable(1.0)
  volume: float                     = Observable(1.0) # Preserved across runs
  playback_time: float | None       = Observable(None)

  file_idx: int | None              = Observable(None)
  file_is_last: bool | None         = Observable(None)
  file_title: str | None            = Observable(None)
  file_duration: float | None       = Observable(None)

  chapter_idx: int | None           = Observable(None) # This is guaranteed to change before the attribute below.
  chapter_title: str | None         = Observable(None)
  chapter_times: list[float | None] = Observable([None]) # Resets to default only after chapter_idx has.

  on_run        = Observable()
  on_start_file = Observable()
  on_end_file   = Observable()

  class Error(Exception):
    pass

  class EndFileReason(Enum):
    Eof = auto()
    User = auto()
    Stop = auto()
    Seek = auto()
    Goto = auto()
    NotPlayable = auto()
    Unknown = auto()

  def __init__(self, *, log=Logger()):
    self.log = log
    self.mpv = None
    self.send_counter = Counter()
    self.send_callbacks = {}

  def __all_observer__(self, name, *args):
    self.log.debug(f'{name} {getattr(self, name)!r} {" ".join(map(repr, args))}')

  def run(self, media, options=[]):
    self.mpv, bm = socket.socketpair()
    self.mpv.settimeout(0.1)

    # Ordering constraint: playback-time -> chapter -> chapter-metadata/by-key/title
    for property in ['pause', 'speed', 'volume', 'playback-time', 'media-title', 'duration', 'chapters', 'chapter', 'chapter-metadata/by-key/title']:
      self.send_async({'command': ['observe_property', 0, property]}, self.check_reply)

    # If you pass more than one media to mpv, file_idx may incorrectly be None for the first one.
    result = subprocess.Popen(['mpv', media, f'--volume={100 * self.volume}'] + options + [f'--input-ipc-client=fd://{bm.fileno()}'], pass_fds=[bm.fileno()])

    was_just_paused = False
    playlist_ids = [] # We are screwed, if the playlist is modified in mpv.
    playlist_id = None
    file_title = None
    chapter_idx = None
    chapter_title = None
    self.is_stopping = False
    self.is_gotoing = False

    def end_file(reason):
      self.notify_observers('on_end_file', reason, playlist_ids[-1] == playlist_id)

      self.is_seeking = True
      self.playback_time = None

      self.file_idx = None
      self.file_is_last = None
      self.file_title = None
      self.file_duration = None

      self.chapter_idx = None
      self.chapter_title = None
      self.chapter_times = [None]

    self.notify_observers('on_run')

    try:
      buf = b''
      while True:
        if not main_thread().is_alive():
          self.stop_async(self.check_reply)

        try:
          buf += self.mpv.recv(4096)
        except TimeoutError:
          if result.poll() is None:
            continue
          else:
            break
        while b'\n' in buf:
          msg, _, buf = buf.partition(b'\n')
          msg = json.loads(msg)
          self.log.debug(f'recv {msg}')

          event = msg.get('event')
          if event is None:
            try:
              self.send_callbacks.pop(msg['request_id'])(msg)
            except Exception as e:
              self.log.warn(self.log.except_msg(e, 'passing mpv reply to send callback'))

          elif event == 'start-file':
            playlist_id = msg['playlist_entry_id']
            if playlist_id not in playlist_ids:
              playlist_ids.append(playlist_id)

            self.notify_observers('on_start_file')

          elif event == 'end-file':
            assert msg['playlist_entry_id'] == playlist_id

            if msg['reason'] == 'eof':
              reason = Mpv.EndFileReason.Eof
            elif msg['reason'] == 'quit':
              reason = Mpv.EndFileReason.Stop if self.is_stopping else Mpv.EndFileReason.User
            elif msg['reason'] == 'stop':
              reason = Mpv.EndFileReason.Goto if self.is_gotoing else Mpv.EndFileReason.User
            elif msg['reason'] == 'redirect' or (msg['reason'] == 'error' and msg['file_error'] == 'no audio or video data played'):
              reason = Mpv.EndFileReason.NotPlayable
            elif msg['reason'] == 'unknown':
              reason = Mpv.EndFileReason.Unknown
            else:
              reason = Mpv.EndFileReason.Unknown
              self.log.warn(f'Cannot recognize end file reason in {msg!r}')

            if 'playlist_insert_id' in msg:
              new_ids = list(range(msg['playlist_insert_id'], msg['playlist_insert_id'] + msg['playlist_insert_num_entries']))
              split = playlist_ids.index(playlist_id) + 1
              playlist_ids = playlist_ids[:split] + new_ids + playlist_ids[split:]

            end_file(reason)

            if reason == Mpv.EndFileReason.NotPlayable:
              playlist_ids.remove(playlist_id)

            was_just_paused = False
            self.is_gotoing = False

          elif event == 'seek':
            # Requesting playback-time here would give us the time we moved to, but we already get that from observe_property.
            self.is_seeking = True
            was_just_paused = False

          elif event == 'playback-restart':
            # We delay notifying observers until we are sure that the playlist entry we are in is actually playable.
            self.file_idx = playlist_ids.index(playlist_id)
            self.file_is_last = playlist_ids[-1] == playlist_id
            self.file_title = file_title
            if self.playback_time is None: # mpv sometimes sends the playback time only after playback-restart for a newly loaded file.
              self.playback_time = 0.0
            self.is_seeking = False
            was_just_paused = False

          elif event == 'property-change':
            name = msg['name']
            if name == 'pause':
              self.is_paused = msg['data']
              was_just_paused = self.is_paused

            elif name == 'speed':
              self.speed = msg['data']

            elif name == 'volume':
              self.volume = msg['data'] / 100

            elif name == 'playback-time':
              if 'data' in msg and not was_just_paused: # mpv sometimes sends this event with bogus values after the player is paused. :(
                self.playback_time = max(0.0, msg['data']) # max(-0.0, 0.0) is -0.0
                # Handle the case: We were at the beginning of a chapter and the chapter metadata was desynchronized.
                if chapter_idx is None or self.chapter_times[chapter_idx + 1] is None or self.playback_time < self.chapter_times[chapter_idx + 1]:
                  self.chapter_idx = chapter_idx
                  self.chapter_title = chapter_title

            elif name == 'media-title':
              file_title = msg.get('data')
              if self.file_idx is not None:
                self.file_title = file_title

            elif name == 'duration':
              self.file_duration = msg.get('data')
              if self.chapter_times[-1] != self.file_duration:
                self.chapter_times[-1] = self.file_duration
                self.notify_observers('chapter_times')

            elif name == 'chapters':
              cnt = msg.get('data', 0)
              self.chapter_times = [None] * cnt + [self.file_duration]
              for i in range(cnt):
                def body(i):
                  def callback(reply):
                    self.chapter_times[i] = reply['data']
                    self.notify_observers('chapter_times')
                  self.send_async({'command': ['get_property', f'chapter-list/{i}/time']}, callback)
                body(i)

            elif name == 'chapter':
              chapter_idx = msg.get('data')
              # Make seeking to the beginning of the current chapter not exit
              # it. By the way, did you know that chapter -1 exists if you seek
              # backwards hard enough?
              if self.chapter_idx is None or chapter_idx is None or chapter_idx != self.chapter_idx - 1 or self.playback_time != self.chapter_times[self.chapter_idx]:
                if chapter_idx is not None and self.chapter_times[chapter_idx] is not None: # Some sanitization for when transitioning from one chapter to the next during normal playback.
                  self.playback_time = max(self.playback_time, self.chapter_times[chapter_idx])
                self.chapter_idx = chapter_idx

            elif name == 'chapter-metadata/by-key/title':
              chapter_title = msg.get('data')
              if chapter_idx == self.chapter_idx: # Both index and title are desynchronized, if the check above fails.
                self.chapter_title = chapter_title

    except KeyboardInterrupt:
      self.log.info('Caught a keyboard interrupt')
    else:
      if self.playback_time is not None:
        end_file(Mpv.EndFileReason.Seek)

    self.mpv.close()
    self.mpv = None
    result.wait()
    return result

  @property
  def is_running(self):
    return self.mpv is not None

  def send_async(self, msg, callback):
    msg['request_id'] = self.send_counter.increment()
    self.send_callbacks[msg['request_id']] = callback
    self.log.debug(f'send {msg}')
    self.mpv.send((json.dumps(msg) + '\n').encode())

  def check_reply(self, reply):
    if reply['error'] != 'success':
      raise Mpv.Error(reply['error'])

  def stop_async(self, callback):
    def intermediate(reply):
      if reply['error'] == 'success':
        self.is_stopping = True
      callback(reply)
    self.send_async({'command': ['quit']}, intermediate)

  def seek_async(self, time, callback):
    self.send_async({'command': ['seek', time, 'absolute', 'exact']}, callback)

  def pause_async(self, callback):
    self.send_async({'command': ['set_property', 'pause', True]}, callback)

  def resume_async(self, callback):
    self.send_async({'command': ['set_property', 'pause', False]}, callback)

  def set_volume_async(self, value, callback):
    self.send_async({'command': ['set_property', 'volume', 100 * value]}, callback)

  def goto_next_async(self, callback):
    def intermediate(reply):
      if reply['error'] == 'success':
        self.is_gotoing = True
      callback(reply)
    self.send_async({'command': ['playlist-next']}, intermediate)

  def goto_prev_async(self, callback):
    def intermediate(reply):
      if reply['error'] == 'success':
        self.is_gotoing = True
      callback(reply)
    self.send_async({'command': ['playlist-prev']}, intermediate)

def unasync(func):
  def result(self, *args):
    future = Future()
    func(self, *args, future.set_result)
    reply = future.result()
    self.check_reply(reply)
    return reply
  return result
for k, v in {k.removesuffix('_async'): unasync(v) for k, v in vars(Mpv).items() if k.endswith('_async')}.items():
  setattr(Mpv, k, v)

def format_playback_state(entry):
  result = format_playback_time(entry['to'])
  if entry['file']['idx'] is not None:
    result += f', file {entry["file"]["idx"] + 1} ‘{entry["file"]["title"]}’'
  if entry['chapter'] is not None:
    result += f', chapter {entry["chapter"]["idx"] + 1} ‘{entry["chapter"]["title"]}’'
  return result

class Player(Mpv):
  def __init__(self, app):
    super().__init__(log=app.log)
    self.app = app
    self.bookmark = None

    def is_seeking():
      if self.is_seeking:
        self.flush_entry(False)
      elif not self.is_paused:
        self.entry['time'] = datetime.now().astimezone().isoformat()
        self.entry['from'] = self.entry['to'] = self.playback_time

    def is_paused():
      if self.is_paused:
        self.flush_entry(False)
      elif not self.is_seeking:
        self.entry['time'] = datetime.now().astimezone().isoformat()
        self.entry['from'] = self.entry['to'] = self.playback_time

    def speed():
      self.flush_entry(True)
      self.entry['speed'] = self.speed

    def playback_time():
      if self.entry['time'] is not None:
        self.entry['to'] = self.playback_time

    def file_idx():
      self.entry['file']['idx'] = None if self.file_idx == 0 and self.file_is_last else self.file_idx

    def file_is_last():
      self.entry['file']['idx'] = None if self.file_idx == 0 and self.file_is_last else self.file_idx

    def file_title():
      self.entry['file']['title'] = self.file_title

    def file_duration():
      self.entry['file']['duration'] = self.file_duration

    def chapter_idx():
      if self.entry['chapter'] is not None:
        self.flush_entry(True)
      if self.chapter_idx is None:
        self.entry['chapter'] = None
      else:
        self.entry['chapter'] = {
          'idx': self.chapter_idx,
          'title': self.chapter_title,
          'start': self.chapter_times[self.chapter_idx],
          'end': self.chapter_times[self.chapter_idx + 1],
        }

    def chapter_title():
      if self.entry['chapter'] is not None:
        self.entry['chapter']['title'] = self.chapter_title

    def chapter_times():
      if self.entry['chapter'] is not None:
        self.entry['chapter']['start'] = self.chapter_times[self.chapter_idx]
        self.entry['chapter']['end'] = self.chapter_times[self.chapter_idx + 1]

    def on_end_file(reason, is_last):
      if reason == Mpv.EndFileReason.Eof:
        self.entry['to'] = self.entry['file']['duration']
      self.flush_entry(False)
      self.is_finished = reason in {Mpv.EndFileReason.Eof, Mpv.EndFileReason.NotPlayable} and is_last

    self.observe('is_seeking', is_seeking)
    self.observe('is_paused', is_paused)
    self.observe('speed', speed)
    self.observe('playback_time', playback_time)

    self.observe('file_idx', file_idx)
    self.observe('file_is_last', file_is_last)
    self.observe('file_title', file_title)
    self.observe('file_duration', file_duration)

    self.observe('chapter_idx', chapter_idx)
    self.observe('chapter_title', chapter_title)
    self.observe('chapter_times', chapter_times)

    self.observe('on_end_file', on_end_file)

  def flush_entry(self, does_playback_continue):
    if self.entry['time'] is None:
      return

    # An example of when the from < to case happens is the user seeking past the end of a file.
    if self.entry['from'] < self.entry['to']:
      self.log.debug(f'flush {self.entry}')
      self.app.save_in_history(deepcopy(self.entry))

    if does_playback_continue:
      self.entry['time'] = datetime.now().astimezone().isoformat()
      self.entry['from'] = self.entry['to']
    else:
      self.entry['time'] = self.entry['from'] = self.entry['to'] = None

  def run(self, media, bookmark, link, should_resume=True):
    self.bookmark = bookmark
    # entry['time'] is not None, if and only if mpv is playing media.
    self.entry = {
      'time': None,
      'type': 'play',
      'bookmark': bookmark.name,
      'file': {
        'idx': None,
        'title': None,
        'duration': None,
      },
      'from': None,
      'to': None,
      'speed': 1.0,
      'chapter': None,
    }
    self.is_finished = False

    if should_resume:
      with self.app.history_lock:
        self.app.load_unfinished_bookmarks()
        playback_state = self.app.unfinished_bookmarks.get(bookmark.slug)
    else:
      playback_state = None
    if playback_state is not None:
      if playback_state['file']['idx'] is None:
        def callback():
          self.seek_async(playback_state['to'], self.check_reply)
          self.unobserve(callback)
        self.observe('file_duration', callback)
      else:
        def callback():
          if self.file_idx is not None:
            if self.file_idx < playback_state['file']['idx']:
              self.goto_next_async(self.check_reply)
            else:
              self.seek_async(playback_state['to'], self.check_reply)
              self.unobserve(callback)
        self.observe('file_idx', callback)

    self.app.save_in_history({'type': 'open' if playback_state is None else 'resume', 'bookmark': bookmark.name, 'link': link})
    result = super().run(media, ['--no-video'] if bookmark.should_discard_video else [])
    self.flush_entry(False)
    if self.is_finished:
      self.log.info(f'Finished {bookmark.name!r}')
      self.app.save_in_history({'type': 'finish', 'bookmark': bookmark.name})

    self.bookmark = None
    return result

  @property
  def is_running(self):
    return self.bookmark is not None
