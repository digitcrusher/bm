# bm - The bookmarks.md manager
# Copyright (C) 2022-2023, 2025 Karol "digitcrusher" Łacina
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os, sys
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from enum import Enum

from common import format_duration, MusicMetadata, slugify
from thumbnails import fetch_thumbnail

# TODO: weekly heatmap

class Criterion(Enum):
  Opens = 'opens'
  PlayTime = 'play-time'

class Category(Enum):
  Titles = 'titles'
  Authors = 'authors'

class Format(Enum):
  Html = 'html'
  Text = 'text'

# TODO: author thumbnails
def ranking(bookmarks, history, criterion=Criterion.PlayTime, year=None, limit=None, category=Category.Titles, format=Format.Text, *, app):
  music_bookmarks = {}
  def callback(bookmark):
    if bookmark.is_music:
      music_bookmarks[bookmark.slug] = bookmark
  bookmarks.root.walk(callback)

  if not music_bookmarks:
    app.log.warn('No music bookmarks found')

  score = {}
  for entry in history:
    if 'bookmark' not in entry or (year is not None and not entry['time'].startswith(f'{year}-')):
      continue
    slug = slugify(entry['bookmark'])
    if slug not in music_bookmarks:
      continue

    if category == Category.Titles:
      key = slug
    elif category == Category.Authors:
      key = music_bookmarks[slug].music_metadata(log=app.log).author
      if key is None:
        continue

    if criterion == Criterion.Opens:
      if entry['type'] == 'open':
        if key not in score:
          score[key] = 0
        score[key] += 1
    elif criterion == Criterion.PlayTime:
      if entry['type'] == 'play':
        if key not in score:
          score[key] = 0
        score[key] += (entry['to'] - entry['from']) / entry['speed']

  if not score:
    app.log.warn('No suitable history data found')

  ranking = sorted(score.items(), key=lambda x: x[1], reverse=True)
  if limit is not None:
    ranking = ranking[:limit]

  if format == Format.Html:
    result = '<!DOCTYPE html>\n'
    result += '<html>\n'
    result += '<head>\n'
    result += '<meta charset="UTF-8">\n'
    result += '<meta name="viewport" content="width=device-width, initial-scale=1">\n'
    with open(os.path.join(sys.path[0], '../res/music_ranking.css'), 'r') as file:
      result += '<style>\n'
      result += file.read()
      result += '</style>\n'
    result += '</head>\n'
    result += '<body>\n'
    if year is not None:
      result += f'<div id="year">{year}</div>\n'
    result += '<main>\n'
  elif format == Format.Text:
    result = f'Your most listened music {category.value}'
    result += ' of all time' if year is None else f' in {year}'
    result += ':\n'

  for position, (item, score) in enumerate(ranking):
    if criterion == Criterion.Opens:
      score_str = f'x{score}'
    elif criterion == Criterion.PlayTime:
      score_str = format_duration(score)

    if format == Format.Html:
      if category == Category.Titles:
        bookmark = music_bookmarks[item]
        metadata = bookmark.music_metadata(log=app.log)
        title = metadata.title
        author = metadata.author if metadata.format != MusicMetadata.Format.Artist else None
        cover = fetch_thumbnail(bookmark, app)
      elif category == Category.Authors:
        title = item
        author = None
        cover = None

      size = 100 + 50 * score / ranking[0][1]
      result += f'<div class="bookmark" style="font-size: {size}%;">\n'
      result += f'<div class="position"><span>{position + 1}</span></div>\n'
      result += '<div class="middle">\n'
      if cover is not None:
        result += f'<img class="cover" src="{cover}">\n'
      else:
        result += f'<div class="cover"><span>?</span></div>\n'
      result += f'<div class="title">{title}</div>\n'
      result += f'<div class="author">{author or ""}</div>\n'
      result += '</div>\n'
      result += f'<div class="score"><span>{score_str}</span></div>\n'
      result += '</div>\n'

    elif format == Format.Text:
      if category == Category.Titles:
        name = music_bookmarks[item].name
      elif category == Category.Authors:
        name = item
      result += f'{position + 1}. {name} at {score_str}\n'

  if format == Format.Html:
    result += '</main>\n'
    result += '</body>\n'
    result += '</html>\n'

  return result

class Timescale(Enum):
  Days = 'days'
  Weeks = 'weeks'
  Months = 'months'

# IDEA: html
def activity(bookmarks, history, criterion=Criterion.PlayTime, year=None, limit=None, timescale=Timescale.Months, *, app):
  music_bookmarks = {}
  def callback(bookmark):
    if bookmark.is_music:
      music_bookmarks[bookmark.slug] = bookmark
  bookmarks.root.walk(callback)

  if not music_bookmarks:
    app.log.warn('No music bookmarks found')

  score = {}
  for entry in history:
    if 'bookmark' not in entry or (year is not None and not entry['time'].startswith(f'{year}-')):
      continue
    slug = slugify(entry['bookmark'])
    if slug not in music_bookmarks:
      continue

    date = datetime.fromisoformat(entry['time']).date()
    if timescale == Timescale.Days:
      key = date
    elif timescale == Timescale.Weeks:
      key = (date - timedelta(days=date.weekday()))
    elif timescale == Timescale.Months:
      key = date.replace(day=1)

    if criterion == Criterion.Opens:
      if entry['type'] == 'open':
        if key not in score:
          score[key] = 0
        score[key] += 1
    elif criterion == Criterion.PlayTime:
      if entry['type'] == 'play':
        if key not in score:
          score[key] = 0
        score[key] += (entry['to'] - entry['from']) / entry['speed']

  if not score:
    app.log.warn('No suitable history data found')

  graph = []
  i = min(score.keys())
  while i <= max(score.keys()):
    graph.append((i, score.get(i, 0)))
    if timescale == Timescale.Days:
      i += timedelta(days=1)
    elif timescale == Timescale.Weeks:
      i += timedelta(weeks=1)
    elif timescale == Timescale.Months:
      i += relativedelta(months=1)

  if limit is not None:
    graph = graph[-limit:]

  max_score = max(i for _, i in graph)

  table = []
  for date, score in graph:
    month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'][date.month - 1]
    if timescale == Timescale.Months:
      label = f'{month} {date.year}'
    else:
      label = f'{date.day} {month[:3]} {date.year}'

    if criterion == Criterion.Opens:
      score_str = f'x{score}'
    elif criterion == Criterion.PlayTime:
      score_str = format_duration(score)

    bar = score / max_score

    table.append((label, score_str, bar))

  screen_width = 80

  title = 'Your listening activity over '
  if limit is not None:
    title += f'the last {limit} '
  title += timescale.value
  title += ' of all time' if year is None else f' in {year}'
  result = title.center(screen_width) + '\n'

  max_label_len = max(len(i) for i, _, _ in table)
  max_score_len = max(len(i) for _, i, _ in table)
  avail_width = screen_width - max_label_len - max_score_len - 6

  result += '─' * max_label_len + '─┬─' + '─' * max_score_len + '─┬─' + '─' * avail_width + '\n'

  for label, score, bar in table:
    bar *= avail_width
    bar = '█' * int(bar) + ' ▏▎▍▌▋▊▉█'[round(bar % 1 * 8)].strip()
    result += f'{label.rjust(max_label_len)} │ {score.rjust(max_score_len)} │ {bar}\n'

  return result
