# bm - The bookmarks.md manager
# Copyright (C) 2022-2023, 2025 Karol "digitcrusher" Łacina
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import gi, os, shutil, subprocess, sys
gi.require_version('Adw', '1')
gi.require_version('Gtk', '4.0')
from concurrent.futures import Future
from dataclasses import dataclass
from gi.repository import Adw, Gdk, Gio, GLib, GObject, Gtk, Pango
from threading import Thread
from time import sleep
from traceback import format_exception
from urllib.parse import urlparse

import downloads
from app import App
from common import Counter, fuzzy_match, is_url, Logger
from downloads import cleanup_download, complete_download, download_with_ytdl, YtdlError
from model import Bookmark, Category
from play import format_playback_time, is_file_playable, Player

# BUG: filter out empty categories when searching bookmarks

class GuiApp(Adw.Application):
  def __init__(self, app):
    super().__init__(application_id=None if app.opt.dev else 'io.lacina.bm')
    GLib.set_application_name('bm')
    self.connect('startup', self.on_startup)
    self.app = app
    self.downloads = {}
    self.player = Player(self)

  def on_startup(self, _): # Overriding do_startup segfaults…
    self.load_config()
    self.bookmarks = self.read_bookmarks()
    self.window = ApplicationWindow(self)

    quit = Gio.SimpleAction.new('quit')
    quit.connect('activate', lambda *_: self.quit())
    bookmarks_file = Gio.SimpleAction.new('bookmarks-file')
    bookmarks_file.connect('activate', lambda *_: subprocess.run(['xdg-open', self.cfg.bookmarks]))
    self.add_action(quit)
    self.add_action(bookmarks_file)

  def do_activate(self):
    self.window.present()

  @property
  def log(self):
    return self.app.log

  @property
  def opt(self):
    return self.app.opt

  @property
  def cfg(self):
    return self.app.cfg

  @property
  def history(self):
    return self.app.history

  @property
  def unfinished_bookmarks(self):
    return self.app.unfinished_bookmarks

  @property
  def history_lock(self):
    return self.app.history_lock

  def load_config(self, *args):
    return self.app.load_config(*args)

  def save_config(self, *args):
    return self.app.save_config(*args)

  def read_bookmarks(self, *args):
    return self.app.read_bookmarks(*args)

  def load_history(self, *args):
    return self.app.load_history(*args)

  def load_unfinished_bookmarks(self, *args):
    return self.app.load_unfinished_bookmarks(*args)

  def save_in_history(self, *args):
    return self.app.save_in_history(*args)

class ApplicationWindow(Gtk.ApplicationWindow):
  def __init__(self, app):
    super().__init__(application=app, title='bm', hide_on_close=not app.opt.dev, default_width=900, default_height=700)

    search_entry = Gtk.SearchEntry(max_width_chars=88)
    def search_changed(_):
      self.bookmarks_tree.search_text = search_entry.get_text()
      if not search_entry.get_text():
        search_button.set_active(False)
    def stop_search(_):
      search_button.set_active(False)
    def changed(_):
      if search_entry.get_text():
        search_button.set_active(True)
        search_entry.grab_focus()
    search_entry.connect('search_changed', search_changed)
    search_entry.connect('stop_search', stop_search)
    search_entry.connect('changed', changed)
    search_entry.set_key_capture_widget(self)

    search_button = Gtk.ToggleButton(icon_name='system-search-symbolic', tooltip_text='Search Bookmarks')
    def toggled(_):
      if search_button.get_active():
        search_entry.grab_focus()
      else:
        search_entry.set_text('')
    search_button.connect('toggled', toggled)

    title = Gtk.Label(css_classes=['title'])
    self.bind_property('title', title, 'label', GObject.BindingFlags.SYNC_CREATE)

    stack = Gtk.Stack(transition_type=Gtk.StackTransitionType.CROSSFADE)
    stack.add_child(title)
    stack.add_child(search_entry)
    stack.bind_property(
      'visible_child', search_button, 'active', GObject.BindingFlags.BIDIRECTIONAL,
      lambda _, visible_child: visible_child == search_entry,
      lambda _, active: search_entry if active else title,
    )

    menu = Gio.Menu()
    menu.append('_Open Bookmarks File', 'app.bookmarks-file')
    menu.append('_Quit bm', 'app.quit')
    menu_button = Gtk.MenuButton(menu_model=menu, icon_name='open-menu-symbolic', tooltip_text='Main Menu')

    header = Gtk.HeaderBar(title_widget=stack)
    header.pack_start(search_button)
    header.pack_end(menu_button)
    self.set_titlebar(header)

    shortcuts = Gtk.ShortcutController()
    shortcuts.add_shortcut(Gtk.Shortcut.new(Gtk.ShortcutTrigger.parse_string('<Ctrl>F'), Gtk.CallbackAction.new(lambda *_: search_button.set_active(not search_button.get_active()))))
    self.add_controller(shortcuts)

    self.bookmarks_tree = BookmarksTree(app.bookmarks, app)
    self.bookmark_view = BookmarkView(app=app)
    def notify_bookmark_view(*_):
      self.bookmark_view.bookmark = self.bookmarks_tree.selected_node if isinstance(self.bookmarks_tree.selected_node, Bookmark) else None
    self.bookmarks_tree.get_model().connect('selection_changed', notify_bookmark_view)
    self.bookmarks_tree.get_model().connect('items_changed', notify_bookmark_view)

    overlay = Gtk.Overlay(child=Gtk.Paned(
      start_child=Gtk.ScrolledWindow(child=self.bookmarks_tree),
      end_child=Gtk.ScrolledWindow(child=self.bookmark_view),
      position=self.get_default_size().width / 2,
    ))
    self.player_controls = PlayerControls(app.player, app, halign=Gtk.Align.CENTER, valign=Gtk.Align.END, margin_bottom=10)
    overlay.add_overlay(self.player_controls)
    self.set_child(overlay)

class BookmarksTree(Gtk.ListView):
  def __init__(self, bookmarks, app):
    super().__init__()
    self.bookmarks = bookmarks
    self.app = app
    self._search_text = ''

    class NodeObject(GObject.Object):
      node = GObject.property(type=object)

    def does_match_search(node_obj):
      node = node_obj.node
      return not isinstance(node, Bookmark) or fuzzy_match(self.search_text, node.name) is not None
    self.filter = Gtk.CustomFilter.new(does_match_search)

    def expand_category(node_obj):
      category = node_obj.node
      if not isinstance(category, Category):
        return None
      items = Gio.ListStore()
      for item in category:
        items.append(NodeObject(node=item))
      return Gtk.FilterListModel.new(items, self.filter)

    root = Gio.ListStore()
    for item in bookmarks.root:
      root.append(NodeObject(node=item))
    self.set_model(Gtk.SingleSelection.new(Gtk.TreeListModel.new(root, False, True, expand_category)))

    def setup(_, item):
      expander = Gtk.TreeExpander(child=Gtk.Label())
      gesture = Gtk.GestureClick()
      gesture.connect('pressed', lambda *_: expander.activate_action('listitem.toggle-expand'))
      expander.add_controller(gesture)
      item.set_child(expander)
      # item.set_focusable(False) # HACK: Enable when GTK 4.12+ arrives in Debian.

    def bind(_, item):
      expander = item.get_child()
      expander.set_list_row(item.get_item())
      node = item.get_item().get_item().node

      label = expander.get_child()
      name = node.name
      if not isinstance(node, Bookmark):
        label.set_text(name)
        return

      match = fuzzy_match(self.search_text, name)
      if match is None:
        label.set_markup('<i>Filtered out bookmark</i>')
        return
      is_matched = match[1]
      split = name.rfind(' (') if node.is_music and name.endswith(')') else -1

      markup = ''
      for i, c in enumerate(name):
        if i == split:
          markup += '<span alpha="50%" size="smaller">'
        c = GLib.markup_escape_text(c)
        markup += f'<b>{c}</b>' if is_matched[i] else c
      if split != -1:
        markup += '</span>'
      label.set_markup(markup)

    factory = Gtk.SignalListItemFactory()
    factory.connect('setup', setup)
    factory.connect('bind', bind)
    self.set_factory(factory)

  @property
  def search_text(self):
    return self._search_text

  @search_text.setter
  def search_text(self, value):
    self._search_text = value
    self.get_parent().get_vadjustment().set_value(0) # BUG: We shouldn't be forced to do this…
    self.filter.changed(Gtk.FilterChange.DIFFERENT)
    self.get_model().items_changed(0, 0, 0)

  @property
  def selected_node(self):
    return self.get_model().get_selected_item().get_item().node

class BookmarkView(Gtk.Box):
  def __init__(self, bookmark=None, *, app):
    super().__init__(orientation=Gtk.Orientation.VERTICAL, margin_start=30, margin_end=30, margin_top=20, margin_bottom=20, spacing=20)
    self.app = app

    self.playback_state = SegmentedLabel({'wrap': True, 'use_markup': True}, margin_start=18, margin_end=18)
    self.finish = Gtk.Button(label='Finish', css_classes=['destructive-action'], valign=Gtk.Align.CENTER)
    def clicked(_):
      app.save_in_history({'type': 'finish', 'bookmark': self.bookmark.name})
      self.refresh_playback_row()
    self.finish.connect('clicked', clicked)
    box = Gtk.Box(layout_manager=BookmarkView.RowLayout())
    box.append(self.playback_state)
    box.append(self.finish)
    self.playback_row = Gtk.ListBoxRow(child=box, activatable=False, focusable=False)
    app.player.observe('on_run', self.refresh_playback_row)

    self.rows = Gtk.ListBox(css_classes=['rich-list'], selection_mode=Gtk.SelectionMode.NONE, show_separators=True)
    self.append(Gtk.Frame(child=self.rows))

    self.bookmark = bookmark

  @property
  def bookmark(self):
    return self._bookmark

  @bookmark.setter
  def bookmark(self, bookmark):
    self._bookmark = bookmark

    while (i := self.rows.get_last_child()) is not None:
      self.rows.remove(i)

    if bookmark is not None:
      self.refresh_playback_row()
      self.rows.append(self.playback_row)
      for site, target in bookmark.links:
        self.rows.append(LinkRow(site, target, bookmark, self.app))

    self.rows.get_parent().set_visible(bookmark is not None)

  def refresh_playback_row(self):
    if self.bookmark is None:
      return None

    if self.app.player.bookmark is not None and self.app.player.bookmark.slug == self.bookmark.slug:
      self.playback_state.set_segments(['Currently playing'])
      self.playback_row.set_sensitive(False)
      return

    with self.app.history_lock:
      self.app.load_unfinished_bookmarks()
      state = self.app.unfinished_bookmarks.get(self.bookmark.slug)
    if state is None:
      self.playback_state.set_segments(['No playback state saved'])
      self.playback_row.set_sensitive(False)
      return

    segments = [f'Paused at {format_playback_time(state["to"])}']
    if state['file']['idx'] is not None:
      segments[-1] += f', file {state["file"]["idx"] + 1} '
      segments.append(f'<i>{GLib.markup_escape_text(state["file"]["title"])}</i>')
      segments.append('')
    if state['chapter'] is not None:
      segments[-1] += f', chapter {state["chapter"]["idx"] + 1} '
      segments.append(f'<i>{GLib.markup_escape_text(state["chapter"]["title"])}</i>')
    if not segments[-1]:
      del segments[-1]
    self.playback_state.set_segments(segments)
    self.finish.set_sensitive(True)
    self.playback_row.set_sensitive(True)

  def refresh_link_rows(self):
    i = self.rows.get_first_child()
    while (i := i.get_next_sibling()) is not None:
      i.refresh()

  class RowLayout(Gtk.LayoutManager):
    def do_get_request_mode(self, _):
      return Gtk.SizeRequestMode.HEIGHT_FOR_WIDTH

    def allocate_width(self, width, a, b):
      a_min, a_nat = a.get_preferred_size()
      b_min, b_nat = b.get_preferred_size()
      if width >= a_nat.width + b_nat.width:
        return a_nat.width, b_nat.width
      a_potential = a_nat.width - a_min.width
      b_potential = b_nat.width - b_min.width
      if a_potential + b_potential > 0:
        extra_width = max(width - a_min.width - b_min.width, 0)
        return (a_min.width + int(extra_width * a_potential / (a_potential + b_potential)),
                b_min.width + int(extra_width * b_potential / (a_potential + b_potential)))
      else:
        return a_min.width, b_min.width

    def do_measure(self, row, orientation, width):
      a = row.get_first_child()
      b = row.get_last_child()
      if orientation == Gtk.Orientation.HORIZONTAL:
        assert width == -1
        a = a.measure(orientation, -1)
        b = b.measure(orientation, -1)
        return a[0] + b[0], a[1] + b[1], -1, -1
      else:
        a_width, b_width = self.allocate_width(2 * (0.7 - 0.3) * width, a, b)
        a = a.measure(orientation, a_width)
        b = b.measure(orientation, b_width)
        return max(a[0], b[0]), max(a[1], b[1]), -1, -1

    def do_allocate(self, row, width, height, _):
      a = row.get_first_child()
      b = row.get_last_child()

      a_min_width = a.get_preferred_size().minimum_size.width
      b_min_width = b.get_preferred_size().minimum_size.width
      a_max_width, b_max_width = self.allocate_width(width, a, b)

      while a_min_width < a_max_width:
        mid = (a_min_width + a_max_width) // 2
        if a.measure(Gtk.Orientation.VERTICAL, mid).natural > height:
          a_min_width = mid + 1
        else:
          a_max_width = mid
      a_width = a_min_width

      while b_min_width < b_max_width:
        mid = (b_min_width + b_max_width) // 2
        if b.measure(Gtk.Orientation.VERTICAL, mid).natural > height:
          b_min_width = mid + 1
        else:
          b_max_width = mid
      b_width = b_min_width

      a_x = 0.3 * width - a_width / 2
      b_x = 0.7 * width - b_width / 2
      overlap = max(a_x + a_width - b_x, 0)
      a_x -= overlap / 2
      b_x += overlap / 2
      if b_x > width - b_width:
        b_x = width - b_width
        a_x = min(a_x, b_x - a_width)
      if a_x < 0:
        a_x = 0
        b_x = max(b_x, a_width)

      rect = Gdk.Rectangle()
      rect.y, rect.height = 0, height
      rect.x, rect.width = a_x, a_width
      a.size_allocate(rect, -1)
      rect.x, rect.width = b_x, b_width
      b.size_allocate(rect, -1)

class SegmentedLabel(Gtk.Widget):
  def __init__(self, segment_props={}, **kwargs):
    super().__init__(layout_manager=self.Layout(), **kwargs)
    self.segment_props = segment_props

  def __del__(self):
    while (i := self.get_first_child()) is not None:
      i.unparent()

  def set_segments(self, segments):
    self.__del__()
    for i in segments:
      Gtk.Label(label=i, justify=Gtk.Justification.CENTER, **self.segment_props).set_parent(self)

  @property
  def children(self):
    i = self.get_first_child()
    while i is not None:
      yield i
      i = i.get_next_sibling()

  class Layout(Gtk.LayoutManager):
    def do_get_request_mode(self, _):
      return Gtk.SizeRequestMode.HEIGHT_FOR_WIDTH

    @dataclass
    class Row:
      width: int
      height: int
      segments: list[Gtk.Label]

    def get_rows(self, label, width):
      result = []
      row = self.Row(0, 0, [])
      for child in label.children:
        nat = child.get_preferred_size().natural_size

        if row.width + nat.width > width and row.segments:
          result.append(row)
          row = self.Row(0, 0, [])

        if nat.width > width:
          min, nat, _, _ = child.measure(Gtk.Orientation.VERTICAL, width)
          assert min == nat
          result.append(self.Row(width, min, [child]))
        else:
          row.width += nat.width
          row.height = max(row.height, nat.height)
          row.segments.append(child)

      if row.segments:
        result.append(row)
      return result

    def do_measure(self, label, orientation, width):
      if orientation == Gtk.Orientation.HORIZONTAL:
        assert width == -1
        min, nat = 0, 0
        for child in label.children:
          child_min, child_nat, _, _ = child.measure(orientation, -1)
          min = max(min, child_min)
          nat += child_nat
        return min, nat, -1, -1

      elif width == -1:
        min, nat = 0, 0
        for child in label.children:
          child_min, child_nat, _, _ = child.measure(orientation, -1)
          min = max(min, child_min)
          nat = max(nat, child_nat)
        return min, nat, -1, -1
      else:
        height = sum(row.height for row in self.get_rows(label, width))
        return height, height, -1, -1

    def do_allocate(self, label, width, height, _):
      rows = self.get_rows(label, width)
      rect = Gdk.Rectangle()
      rect.y = (height - sum(i.height for i in rows)) / 2
      for row in rows:
        rect.height = row.height
        if len(row.segments) == 1:
          rect.x = 0
          rect.width = width
          row.segments[0].size_allocate(rect, -1)
        else:
          rect.x = (width - row.width) / 2
          for segment in row.segments:
            rect.width = segment.get_preferred_size().natural_size.width
            segment.size_allocate(rect, -1)
            rect.x += rect.width
        rect.y += rect.height

class LinkRow(Gtk.ListBoxRow):
  def __init__(self, site, target, bookmark, app):
    super().__init__(child=Gtk.Box(layout_manager=BookmarkView.RowLayout()), activatable=False, focusable=False)
    self.site = site
    self.target = target
    self.bookmark = bookmark
    self.app = app

    link = Gtk.LinkButton(uri=target, label=site)
    if site is None:
      link.get_child().set_text(urlparse(target).netloc if is_url(target) else 'local')
    if not is_url(target):
      link.connect('activate_link', lambda _: link.set_uri(f'file://{target}'))
      link.connect('notify::visited', lambda *_: link.set_uri(target))
    self.get_child().append(link)

    self.buttons = Gtk.Box(css_classes=['linked'], halign=Gtk.Align.CENTER)
    self.progress_bar = Gtk.ProgressBar(valign=Gtk.Align.CENTER, halign=Gtk.Align.CENTER)
    self.right_side = Gtk.Stack(transition_type=Gtk.StackTransitionType.CROSSFADE)
    self.right_side.add_child(self.buttons)
    self.right_side.add_child(self.progress_bar)
    self.get_child().append(self.right_side)
    self.refresh()

  def refresh(self):
    if (progress := self.app.downloads.get(self.target)) is not None:
      self.progress_bar.set_fraction(progress)
      self.right_side.set_visible_child(self.progress_bar)
      return

    while (i := self.buttons.get_last_child()) is not None:
      self.buttons.remove(i)

    if is_url(self.target):
      if not os.path.exists(downloads.path_for(self.target, self.app)) and self.app.cfg.download:
        self.buttons.append(DownloadButton(self.target, self.bookmark, self.app))
      else:
        self.buttons.append(PlayButton(self.site, self.target, self.bookmark, self.app))
      if os.path.exists(downloads.path_for(self.target, self.app)):
        self.buttons.append(ShowDownloadButton(self.target, self.app))
    elif self.bookmark.is_music or is_file_playable(self.target):
      self.buttons.append(PlayButton(self.site, self.target, self.bookmark, self.app))

    self.right_side.set_visible_child(self.buttons)

class DownloadButton(Gtk.Button):
  def __init__(self, url, bookmark, app):
    super().__init__(icon_name='folder-download-symbolic', tooltip_text='Download Media')
    self.connect('clicked', lambda _: Thread(target=self.clicked, daemon=True).start())
    self.url = url
    self.bookmark = bookmark
    self.app = app

  def clicked(self):
    wedge = object()
    if self.app.downloads.setdefault(self.url, wedge) is not wedge: # https://github.com/python/cpython/issues/57730
      return

    self.app.downloads[self.url] = 0.0
    GLib.idle_add(self.app.window.bookmark_view.refresh_link_rows)

    while True:
      try:
        def on_progress(value):
          self.app.downloads[self.url] = value
          GLib.idle_add(self.app.window.bookmark_view.refresh_link_rows)
        download_with_ytdl(self.url, self.bookmark, on_progress, app=self.app)

      except FileNotFoundError:
        GLib.idle_add(ErrorDialog(
          text='Error downloading bookmark',
          secondary_text='yt-dlp is not installed on your system',
          transient_for=self.app.window,
        ).show)

      except YtdlError as e:
        dialog = ErrorDialog(
          text='Error downloading bookmark',
          secondary_text=e.log.rstrip(),
          buttons=Gtk.ButtonsType.NONE,
          transient_for=self.app.window,
        )
        dialog.add_button('_Cancel', 2)
        if e.files:
          dialog.add_button('Call it _done', 1)
        dialog.add_button('_Retry', 0)

        clicked_button = Future()
        dialog.connect('response', lambda _, button: clicked_button.set_result(button))

        GLib.idle_add(dialog.show)
        match clicked_button.result():
          case 0:
            continue
          case 1:
            complete_download(self.url, e.files, self.app)
          case 2:
            cleanup_download(self.url, self.app)
      break

    del self.app.downloads[self.url]
    GLib.idle_add(self.app.window.bookmark_view.refresh_link_rows)

class PlayButton(Gtk.Button):
  def __init__(self, site, target, bookmark, app):
    super().__init__(icon_name='media-playback-start-symbolic', tooltip_text='Play Media')
    self.connect('clicked', lambda _: Thread(target=self.clicked).start())
    self.site = site
    self.target = target
    self.bookmark = bookmark
    self.app = app

  def clicked(self):
    if self.app.player.is_running:
      self.app.player.stop()
    while self.app.player.is_running:
      sleep(0.01)

    download = downloads.path_for(self.target, self.app)
    media = download if os.path.exists(download) else self.target
    try:
      self.app.player.run(media, self.bookmark, (self.site, self.target))
    except Exception as e:
      GLib.idle_add(ErrorDialog(text='Error playing bookmark', exception=e, transient_for=self.app.window).show)

    GLib.idle_add(self.app.window.player_controls.refresh)
    GLib.idle_add(self.app.window.bookmark_view.refresh_playback_row)

class ShowDownloadButton(Gtk.Button):
  def __init__(self, target, app):
    super().__init__(icon_name='folder-open-symbolic', tooltip_text='Open Download Location')
    self.target = target
    self.app = app

  def do_clicked(self):
    try:
      subprocess.Popen(['nautilus', '-s', downloads.path_for(self.target, self.app)], start_new_session=True)
    except Exception as e:
      ErrorDialog(text='Error opening download location', exception=e, transient_for=self.app.window).show()

class PlayerControls(Gtk.Box):
  def __init__(self, player, app, **kwargs):
    super().__init__(orientation=Gtk.Orientation.VERTICAL, css_classes=['osd', 'toolbar'], **kwargs)
    self.player = player
    self.app = app

    self.title = Gtk.Label(css_classes=['heading'], margin_top=9, margin_start=9, margin_end=9, tooltip_text='Bookmark Name', halign=Gtk.Align.CENTER)
    self.subtitle = Gtk.Label(margin_start=9, margin_end=9, tooltip_text='Track Title', halign=Gtk.Align.CENTER)
    self.goto_prev = Gtk.Button(icon_name='media-skip-backward-symbolic', tooltip_text='Previous Track')
    self.pause = Gtk.Button(icon_name='media-playback-pause-symbolic', tooltip_text='Pause')
    self.resume = Gtk.Button(icon_name='media-playback-start-symbolic', tooltip_text='Play')
    self.goto_next = Gtk.Button(icon_name='media-skip-forward-symbolic', tooltip_text='Next Track')
    self.position = Gtk.Label(
      attributes=Pango.AttrList.from_string('0 -1 font-features "tnum"'),
      css_classes=['dim-label'],
      margin_start=2,
      margin_end=2,
      tooltip_text='Track Position',
      valign=Gtk.Align.CENTER,
    )
    self.seek = Gtk.Scale(width_request=200, hexpand=True, tooltip_text='Seek')
    self.seek.set_increments(5, 60)
    def on_draw(_, clock): # Minimal improvement for very short files
      if not self.player.is_seeking and not self.player.is_paused and clock.get_fps() > 1: # Sometimes Gtk reports values close to zero, but not zero, when switching workspaces for example.
        self.seek.set_value(self.seek.get_value() + self.player.speed / clock.get_fps())
      return True
    self.seek.add_tick_callback(on_draw)
    self.duration = Gtk.Label(
      attributes=Pango.AttrList.from_string('0 -1 font-features "tnum"'),
      css_classes=['dim-label'],
      margin_start=2,
      margin_end=2,
      tooltip_text='Track Duration',
      valign=Gtk.Align.CENTER,
    )
    self.volume = Gtk.VolumeButton()
    self.volume.get_adjustment().set_step_increment(0.04)

    self.actions = Gtk.Box(hexpand=True)
    for i in [self.goto_prev, self.pause, self.resume, self.goto_next, None, self.position, self.seek, self.duration, None, self.volume]:
      if i is None:
        i = Gtk.Separator()
        i.add_css_class('spacer')
      self.actions.append(i)
    for i in [self.title, self.subtitle, self.actions]:
      self.append(i)

    def clicked(_):
      if self.player.bookmark.is_music and self.player.chapter_idx is not None and self.player.chapter_idx - 1 >= 0:
        self.player.seek(self.player.chapter_times[self.player.chapter_idx - 1])
      else:
        self.player.goto_prev_async(self.player.check_reply)
    self.goto_prev.connect('clicked', clicked)

    self.pause.connect('clicked', lambda _: self.player.pause())
    self.resume.connect('clicked', lambda _: self.player.resume())

    def clicked(_):
      if self.player.bookmark.is_music and self.player.chapter_idx is not None:
        self.player.seek(self.player.chapter_times[self.player.chapter_idx + 1])
      else:
        self.player.goto_next_async(self.player.check_reply)
    self.goto_next.connect('clicked', clicked)

    def on_run():
      self.unqueued_seek_counter = Counter()
      self.has_unprocessed_seek = False
    on_run()
    self.player.observe('on_run', on_run)
    def change_value(_1, _2, value):
      self.unqueued_seek_counter.increment()
      def callback(reply):
        self.has_unprocessed_seek = True # A command reply means that mpv has queued it, and not that it has executed it.
        self.unqueued_seek_counter.decrement()
        self.player.check_reply(reply)
      self.player.seek_async(max(self.seek.get_adjustment().get_lower(), value), callback)
    self.seek.connect('change_value', change_value)
    def is_seeking():
      self.has_unprocessed_seek &= self.player.is_seeking
    self.player.observe('is_seeking', is_seeking)

    self.volume.set_value(self.player.volume)
    self.volume.connect('value_changed', lambda _, value: self.player.set_volume(value))

    for i in ['is_paused', 'volume', 'playback_time', 'file_idx', 'file_is_last', 'file_title', 'file_duration', 'chapter_idx', 'chapter_title', 'chapter_times']:
      self.player.observe(i, lambda: GLib.idle_add(self.refresh))
    self.refresh()

  def refresh(self):
    mpv = self.player
    bookmark = mpv.bookmark
    is_music = bookmark is not None and bookmark.is_music

    if bookmark is None:
      self.title.set_markup('<i>Unknown bookmark</i>')
    else:
      name = bookmark.name
      self.title.set_text(name.rpartition(' (')[0] if is_music and ' (' in name and name.endswith(')') else name)

    if mpv.is_paused:
      self.resume.set_visible(True)
      if self.pause.has_focus():
        self.resume.grab_focus()
      self.pause.set_visible(False)
    else:
      self.pause.set_visible(True)
      if self.resume.has_focus():
        self.pause.grab_focus()
      self.resume.set_visible(False)

    # We allow gotoing during file transitions to make super fast gotoing possible.
    self.goto_prev.set_sensitive((mpv.file_idx is None or mpv.file_idx - 1 >= 0) or (is_music and mpv.chapter_idx is not None and mpv.chapter_idx - 1 >= 0))
    self.goto_next.set_sensitive(not mpv.file_is_last or (is_music and mpv.chapter_idx is not None and mpv.chapter_idx + 1 < len(mpv.chapter_times) - 1))

    if is_music and mpv.chapter_idx is not None:
      if mpv.chapter_title is None:
        self.subtitle.set_markup('<i>Unknown track</i>')
      else:
        self.subtitle.set_text(mpv.chapter_title)
      start = mpv.chapter_times[mpv.chapter_idx]
      end = mpv.chapter_times[mpv.chapter_idx + 1]
      start = 0 if start is None else start
      end = 0 if end is None else end
    else:
      if mpv.file_title is None:
        self.subtitle.set_markup('<i>Unknown track</i>')
      else:
        self.subtitle.set_text(mpv.file_title)
      start = 0
      end = 0 if mpv.file_duration is None else mpv.file_duration
    self.position.set_text(format_playback_time((0 if mpv.playback_time is None else mpv.playback_time) - start))
    if self.unqueued_seek_counter.value <= 0 and not self.has_unprocessed_seek: # Checking for unprocessed seeks avoids visual jitters when scrolling the seek slider.
      self.seek.set_value(0 if mpv.playback_time is None else mpv.playback_time) # Access the playback time only after we have passed the check to avoid a data race (and thus another jitter).
    self.seek.set_range(start, end)
    self.duration.set_text(format_playback_time(end - start))

    self.volume.set_value(mpv.volume)
    self.set_sensitive(mpv.is_running)

class ErrorDialog(Gtk.MessageDialog):
  def __init__(self, exception=None, **kwargs):
    super().__init__(**({
      'message_type': Gtk.MessageType.ERROR,
      'secondary_text': None if exception is None else ''.join(format_exception(None, exception, exception.__traceback__)).rstrip(),
      'buttons': Gtk.ButtonsType.CLOSE,
      'modal': True,
    } | kwargs))

    box = self.get_message_area()
    text = box.get_last_child()
    box.remove(text)
    scrolled_window = Gtk.ScrolledWindow(
      child=text,
      max_content_width=600,
      max_content_height=400,
      propagate_natural_width=True,
      propagate_natural_height=True,
    )
    box.append(scrolled_window)
    # Prevent the so-called "automatic" vertical scrollbar from influencing
    # the ScrolledWindow's minimum height and making the MessageDialog look
    # like ass, even when the content has no overflow and the scrollbar is
    # not needed.
    #
    # The ScrolledWindow's width maxes out at 540px for some obscure reason.
    if text.measure(Gtk.Orientation.VERTICAL, scrolled_window.get_max_content_width() - 60).natural < scrolled_window.get_max_content_height():
      scrolled_window.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.NEVER)

  def do_response(self, _):
    self.destroy()

def write_desktop(dir=os.path.expanduser('~/.local/share/applications/'), *, app):
  dir = os.path.join(dir, '')
  shutil.copy(os.path.join(sys.path[0], '../res/bm.desktop'), dir)
  app.log.info(f'Copied bm.desktop to {dir!r}')
