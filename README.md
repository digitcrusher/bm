# bm

The bookmarks.md manager - a Python script which takes in a Markdown file (a "bookmarks.md") consisting of headings and lists of links ("bookmarks") and allows you to open those bookmarks using your web browser, mpv or other default applications for files, download bookmarks using yt-dlp, save the playback history of mpv, and generate music rankings from that history.

```markdown
This is an example bookmarks.md to showcase all of the available semantics. All categories, including this unnamed root category, may contain descriptions such as these.

# Category

## Nested category
You can nest categories by prefixing the name with one hash more than the parent category.

- [bm will first try to download URLs using yt-dlp…](https://www.youtube.com/watch?v=hqega6a2g6A)
- […and if it fails, open them using the default system application for URLs.](https://bigrat.monster/)
- [Local files will first be opened with mpv…](/usr/share/sounds/alsa/Noise.wav)
- […and if unsuccessful, using the default application.](/usr/include/linux/kernel.h) [[Oh, hey! You can have multiple links in one bookmark!]](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/include/linux/kernel.h)
- [A bookmark can also have "attributes" (text inside curly brackets) following all links.](https://mirrors.edge.kernel.org/pub/linux/kernel/SillySounds/english.wav) {added 2022-12-31}
- Timestamps from Firefox or Chrome are automatically converted to human-readable UTC dates. {added to Firefox 1672444800} {added to Chrome 13316918400123456}
- [Here's an attribute that removes the video from a bookmark.](https://www.youtube.com/watch?v=B3uku3Fh7I4) {discard video}
- You can also have a bookmark without any links or attributes.

## Music {music}
All bookmarks enclosed in a category with the attribute `music` will be included in music rankings and treated as having the `discard video` attribute by default.

The names of such bookmarks have special meaning and are expected to be of the format `[<author> - ]<title>[ (<format>)]` or `"[<author> - ]<title>"[ (<format>)]`, where square brackets denote optional text. `<format>` may be one of `artist`, `song`, `album`, `extended play`, `song compilation`, `album compilation`, `extended play compilation`, or `live song compilation`.

The links are also used to find thumbnails for the bookmarks and the search order for thumbnails is `local` (a directory with a file whose name contains the case-insensitive string `cover`), `website` (only for aphextwin.warp.net), `Bandcamp`, the bookmark's name on MusicBrainz, `YouTube`, and the bookmark's name again but on Discogs (provided you supply your API token in the `DISCOGS_TOKEN` environment variable).

- [The best and only supported music player](https://mpv.io/) {not music}

### Some examples
- Autechre - Bike (song) [[YouTube]](https://www.youtube.com/watch?v=qkTJTf7Yvk8)
- steventhedreamer (artist) [[SoundCloud]](https://soundcloud.com/steventhedreamer/) [[Bandcamp]](https://steventhedreamer.bandcamp.com/)
- "Texas Faggott - Back to Mad EP" (extended play) [[YouTube]](https://www.youtube.com/playlist?list=OLAK5uy_ljjbQpjg13pUIedSTzN3qJJgm8KLY9EPc)
- VulgarGrad - The Odessa Job (album) [[Bandcamp]](https://vulgargrad.bandcamp.com/album/the-odessa-job)
- Aphex Twin - T69 Collapse (song) [[YouTube]](https://www.youtube.com/watch?v=SqayDnQ2wmw) {keep video}
- Klaus Schulze - Kontinuum (album) [[YouTube]](https://www.youtube.com/watch?v=UkXeOMSwdZQ)
```

## Setup

For a user-specific installation:
1. `mkdir ~/.local/opt/`
1. `cd ~/.local/opt/`
2. `git clone https://gitlab.com/digitcrusher/bm.git`
3. `ln -s ~/.local/opt/bm/src/main.py ~/.local/bin/bm`
4. `bm wd`

For a system-wide installation:
1. `cd /opt/`
2. `sudo git clone https://gitlab.com/digitcrusher/bm.git`
3. `sudo ln -s /opt/bm/src/main.py /usr/local/bin/bm`
4. `sudo bm wd /usr/local/share/applications/`

Continuing for both cases:
1. Make sure you have Python 3, mpv, yt-dlp and [PyGObject (optional)](https://pygobject.gnome.org/getting_started.html) installed on your system.
2. Install PyYAML using `pip3 install -r requirements.txt`.
3. Set the path to your bookmarks.md using `bm c -b <file>`.
4. See what you can do with bm by running `bm` and enjoy.

I'll keep maintaining bm as long as I continue using it myself, and release updates from time to time, which you'll be able to download with `git pull` or `sudo git pull`.

If you are getting "Access denied" errors for cover images when opening a music ranking in a web browser, then that's probably because you're using Snap, which has a hardcoded limitation that prevents applications from accessing dot directories in home folders. My solution to that problem was uninstalling Firefox (my preferred web browser) from Snap and installing it again from Mozilla's PPA.
